This repository contains some developments work done in the context of the HardBlare project, funded by the Labex CominLabs.

If some code is taken from another repository or git, the link is mentioned explicitly otherwise the code was written by HardBlare members.

Currently, the folders/files this repository contains are explained in the following table.

| File/**Folder** name | Desription | Remarks |
| ---------------- | ---------- | ------- |
| hardware_designs | This folder contains some hardware projects done. The details about each core is explained in the README file contained inside the folder. | |
| Software | This folder contains the C development done. The README file inside the folder contains more details. | |
| README.md | This file. | |

