This file explains each file or folder contained in it. 

| File/**Folder** name | Desription | Remarks | 
| ---------------- | ---------- | ------- |
| **PFT_Decoder_C** | The C code for the PFT decoder. It is useful to decode raw trace recovered from ETB or from the TPIU design without any PFT Decoder. | The usage is explained in the folder. The output is in ANSI escape sequences to ease the reading of different trace packets :smiley: |
| patches.md | This file contains all patches developed for the CoreSight components and that are used in order to recover the trace on the FPGA from the TPIU. | Patches are generated on Linux kernel v4.7. |
| **cross_compiler_mips** | This folder contains the compiler used for the dispatcher. | |
| main_code_dift_coprocessor.c | This file contains the simple code used on the dispatcher. | This code has been tested in simulation and on the FPGA. | 
| **Benchmark** | This folder contains benchmarks used to evaluate the communication overhead for DIFT. | |
| **TMC** | This folder contains the software code used in order to test the TMC core in simulations. | |
