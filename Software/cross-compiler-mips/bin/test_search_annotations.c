#define Xil_In32(Addr)  		(*(volatile unsigned *)(Addr))
#define Xil_Out32(Addr, Value)  	(*(volatile unsigned *)((Addr)) = (Value))
#define ANNOTATION_ADDRESS 		(0x10000)
#define ANNOTATION_BB_ADDRESS		(0x20000)
// Search annotations for given address in argument and return the address of anntoations in memory for corresponding address
static inline unsigned int search_annotations_address(unsigned int address){
	unsigned int addr, i, ret;
	unsigned jump_table_size = Xil_In32(ANNOTATION_ADDRESS+0);
	// read jump table
	for (i = 1; i < jump_table_size; i+=2){
		if (address == Xil_In32(ANNOTATION_ADDRESS + i*4)){
			ret = Xil_In32(ANNOTATION_ADDRESS+(i+1)*4);
			break;
		}
	}
	return ret;
}

// Store annotations in memory for the corresponding trace 
static inline void store_annotations_memory(unsigned int offset, unsigned int size){
	unsigned int i, annotation; 
	unsigned write_address;
	unsigned annotation_offset = ANNOTATION_ADDRESS + offset*4;
	for (i = 0; i < size; i++){
		annotation = Xil_In32(annotation_offset + i*4);
		write_address = ANNOTATION_BB_ADDRESS + i*4;
		Xil_Out32(write_address, annotation);
	}
}

int main (void){
	unsigned int annotation_size, annotation_offset;	
	annotation_offset = search_annotations_address(0x10450);
	// Determine annotation size by reading header at annotation_address
	annotation_size = Xil_In32(ANNOTATION_ADDRESS+annotation_offset*4);
	annotation_offset += 1;
	// Store annotations in memory for second stage
	store_annotations_memory(annotation_offset, annotation_size);
}

