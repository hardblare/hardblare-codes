#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 4

void wht_bfly (long * a, long * b)
{
	long tmp = *a;
	*a += *b;
	*b = tmp - *b;
}

// just a integer log2
int log2_f (long x)
{
	int l2;
	for (l2 = 0; x > 0; x >>=1) ++ l2;
	return (l2);
}

////////////////////////////////////////////
// Fast in-place Walsh-Hadamard Transform //
////////////////////////////////////////////

void fwht (long * data, int n)
{
  const int log2 = log2_f (n) - 1;
  unsigned int i, j, k;
  for (i = 0; i < log2; ++i)
    for (j = 0; j < (1 << log2); j += 1 << (i+1))
       for ( k = 0; k < (1<<i); ++k)
           wht_bfly (&data [j + k], &data [j + k + (1<<i)]);
}

void app_wht()
{
	static long data[SIZE];

	unsigned int i;

	srand(time(NULL));

	for ( i = 0; i < SIZE; i++ )
		data[i] = rand();

	fwht(data, SIZE);
}
