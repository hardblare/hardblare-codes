binary_name=crc
echo $binary_name
file1="/home/muhammad/Documents/custom_benchmark/benchmark/$binary_name/$binary_name.bb"
file2="/home/muhammad/Documents/custom_benchmark/benchmark/$binary_name/output"
#awk 'BEGIN {OFS=" "}{
#  getline line < "$file2"
#  print $0,line
#} ' "$file1"
#"paste $file2 $file1"
awk -v OFS='\t' 'FNR==NR{id=$1;$1="";a[id]=$0;next} ($1 in a) {id2=$1;$1="";print id2,$0";",a[id2]} !($1 in a){print $0}' "$file2" "$file1" > annotations.bb

#awk 'NR==FNR{a[NR]=$0;next}{print a[FNR], ";", $0}' $file2 $file1 > annotations.bb
# find basic block annotations
grep -c '^MB No.' annotations.bb
# store the starting address of basic block in the list to construct jumpt table
bb_addr_list=( $(awk '/^MB No./{print $8}' annotations.bb) )
printf '%s\n\n' "${bb_addr_list[@]}" > annotations.basic_block_list
sed -i.bak2 '/^\s*$/d' annotations.basic_block_list
# The annotations.bb file is constructed so that we can use the ";" FS (field separator) to find the annotations for each CPU instruction
# gawk -F ";" '{if ($0 ~ /^MB No./) print pat1=$8; if ($0 ~ /^0x/) print $3}' annotations.bb 
# awk -F ";" 'BEGIN{ORS="";} {if ($0 ~ /^MB No./) print pat1=$8; if ($0 ~ /^0x/) {gsub(" ","\n",$3); print $3;}}' annotations.bb

# We get all the annotations of each basic block with some additional text that will be sorted out later :)
# The annotations for push and pop (or other isntructions that generate multiple annotations) are printed by replacing white space with a newline (This is done by using gsub function)

# awk -F ";" 'BEGIN{ORS="";} {if ($0 ~ /^MB No./) print "\n"$0; if ($0 ~ /^0x/) {gsub(" ","\n",$2); print $2;}}' annotations.bb > annotations.formatted
awk -F ";" 'BEGIN{ORS="\n";} {if ($0 ~ /^MB No./) print "\n"$0; if ($0 ~ /^0x/) {gsub(" ","\n",$2); print $2;}}' annotations.bb > annotations.formatted
awk '$1=$1' annotations.formatted > annotations.formatted2 
mv annotations.formatted2 annotations.formatted

cat annotations.formatted | sed 's/^[ \t]*//' > annotations.formatted2
mv annotations.formatted2 annotations.formatted

# deltete zeroes inside each basic block. We create a backup file in case if something goes wrong
#sed -i.bak '/^00000000/d' annotations.formatted
# delete also empty lines 
sed -i.bak2 '/^\s*$/d' annotations.formatted

awk '/MB/{print} !/^MB/{out=""; 
for(i=1;i<=NF;i++){out=out"\n"$i}; print out}' annotations.formatted > annotations.formatted2
mv annotations.formatted2 annotations.formatted
# delete also empty lines 
sed -i.bak2 '/^\s*$/d' annotations.formatted

# get BB number from field 3 
awk '/MB/{sub(/.$/, "", $3);print $3}' annotations.formatted

# Count the number of annotations in each basic block. The first value is not relevant.
# count_annotations_bb is a list of counts of annotations in each basic block
# count_annotations_bb=( $(awk '/^MB No./ {if (NR>1)print count; ; count=0; next} {if ($0 ~ /^0x/) count++} END {print count}' annotations.bb) )
# count_annotations_bb=( $(wk '/^MB No./{print $NF}' annotations.bb | grep -o '[0-9]\+') )
count_annotations_bb=( $(awk '/^MB No./ {if (NR>1)print count; ; count=1; next} {count++} END {print count}' annotations.formatted) )

# Debugging the list (print the list)
printf '%s\n' "${count_annotations_bb[@]}"
#awk '/^MB No./ {if (NR>1)print count; ; count=1; next} {count++} END {print count}' annotations.formatted
# count number of annotations
total_nb_annotations=$(awk '/^MB No./ {if (NR>1)print count; ; count=1; next} {count++} END {print count}' annotations.formatted | jq -s 'add') 
# temporary variables to store locations of each element in memory
jump_table_size=$(printf '%s\n\n' "${bb_addr_list[@]}" | wc -l) ## The "\n\n" is important in order to count correctly :) 
# The first value in jump table is the size of jump table
annotations_start=$((jump_table_size + 1 )) # The first variable holds jump_table_size

# compute start address of each basic block annotations in memory
awk '/^MB No./ {if (NR>1)print count; ; count=1; next} {count++} END {print count}' annotations.formatted > annotations.numbers

# annotations offset is obtained by adding 1 to each count of basic block annotations
awk '{print(total = $0)}' annotations.numbers > annotations.offsets
# sed -i -e '1i0' annotations.offsets # add 0 offset
# number of instructions in each 
# awk '{print(total = $0)}' annotations.numbers > annotations.numbers
# offsets=( $(awk '!/0/{print(total += $0)} /0/{print $0}' annotations.numbers) )
# number_of_instructions=( $(awk '{print($0)}' annotations.numbers) )

# global offsets required for jump table 
# global_offsets=( $(for i in $(cat annotations.offsets); do echo if [ $i -ne 0 ]; then echo "$((i+annotations_start))"; done) )
#global_offsets=( $(for i in $(cat annotations.offsets); do if [ $i -eq 0 ]; then echo "$i"; else echo "$((i+annotations_start))"; fi; done) )
# echo "${global_offsets[@]}"
# echo "${number_of_instructions[@]}"
# double checking everything is ok.
# printf '%s\n' "${global_offsets[@]}" | wc -l

# Now the top of the file can be constructed :) 
# get the length of array 
# echo ${#global_offsets[@]}
# length=$(echo ${#bb_addr_list[@]})


./top_annotations.o > annotations.top
./modify_annotations.o > annotations.bottom
# delete also empty lines 
sed -i.bak2 '/^\s*$/d' annotations.top
# # add text to a file  
{ cat annotations.top; cat annotations.bottom; } > annotations.memory

# # print (1 + jump_table_size) lines of the file 
# rm top.temp
# printf "%x\n" $jump_table_size >> top.temp
# for (( i=1; i<$length; i++ ));
# do
#   printf "%s\n%x\n" ${bb_addr_list[i]} ${global_offsets[i]} >> top.temp;
# done

# # for (( i=1; i<$length; i++ ));
# # do
# #   printf "%s\n%x\n" ${bb_addr_list[i]} ${global_offsets[i]} >> top.temp;
# # done

# # add text to a file  
# { cat top.temp ; cat annotations.formatted; } > annotations.memory

# awk '{ if (NR == 4) print "different"; else print $0}' annotations.memory

# offsets=( $(awk '{print(total = $0 + 1)}' annotations.numbers) )


# FNR == NR {
#   />/ && idx[FNR] = ++i
#   $2 || val[i] = $1      # Store source location
#   next
# }

# mylist=( $(awk 's/^MB No./{print $1}' annotations.bb) )


# gawk -F[.:] '{if ($0 ~ /^MB No./) pat1=$8; if ($0 ~ /^0x/) pat2=$NF}{if (pat1 && pat2) print pat1, pat2}' file

# gawk -F[.:] '{if ($0 ~ /pattern1/) pat1=$1; if ($0 ~ /pattern2/) pat2=$2}' file

# awk '/^MB No./ {if (NR>1)print count; print; count=0; next} {count++} END {print count}' file
