Disassembly using symbol table of file: /home/muhammad/Documents/custom_benchmark/benchmark/fir/fir.elf
Section Name: .text
**************************************
MB No. 0, Type: 2. Starts at 0x100e0 / BB count. 1, Total inst count 4: 
Direct succ: 0 /Remote succ: 50
Indirect succ: 1 
Direct pred:  /Indirect pred: 
0x100e0:	push		{r4, sl, fp, lr} 
0x100e4:	add		fp, sp, #8 
0x100e8:	mov		r4, r0 
0x100ec:	bl		#0x109a4 
Direct branch: 1, Conditional: 0, Target: 0x109a4
**************************************
MB No. 1, Type: 2. Starts at 0x100f0 / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 48
Indirect succ: 2 
Direct pred:  /Indirect pred: 
0x100f0:	bl		#0x1096c 
Direct branch: 1, Conditional: 0, Target: 0x1096c
**************************************
MB No. 2, Type: 2. Starts at 0x100f4 / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 50
Indirect succ: 3 
Direct pred:  /Indirect pred: 
0x100f4:	bl		#0x109a4 
Direct branch: 1, Conditional: 0, Target: 0x109a4
**************************************
MB No. 3, Type: 2. Starts at 0x100f8 / BB count. 1, Total inst count 3: 
Direct succ: 0 /Remote succ: 126
Indirect succ: 
Direct pred:  /Indirect pred: 
0x100f8:	mov		r0, r4 
0x100fc:	mov		lr, pc 
0x10100:	b		#0x10fac 
Direct branch: 1, Conditional: 0, Target: 0x10fac
**************************************
MB No. 4, Type: 2. Starts at 0x10104 / BB count. 1, Total inst count 8: 
Direct succ: 0 /Remote succ: 5
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10104:	mov		fp, #0 
0x10108:	mov		lr, #0 
0x1010c:	ldr		r1, [pc, #0x10] 
0x10110:	add		r1, pc, r1 
0x10114:	mov		r0, sp 
0x10118:	bic		ip, r0, #0xf 
0x1011c:	mov		sp, ip 
0x10120:	bl		#0x10128 
Direct branch: 1, Conditional: 0, Target: 0x10128
**************************************
MB No. 5, Type: 2. Starts at 0x10128 / BB count. 1, Total inst count 16: 
Direct succ: 0 /Remote succ: 44
Indirect succ: 
Direct pred: 4  /Indirect pred: 
0x10128:	push		{fp, lr} 
0x1012c:	mov		fp, sp 
0x10130:	sub		sp, sp, #8 
0x10134:	mov		r2, r0 
0x10138:	movw		r3, #0x1734 
0x1013c:	ldr		r1, [r2], #4 
0x10140:	movt		r3, #1 
0x10144:	mov		r0, #0 
0x10148:	str		r3, [sp] 
0x1014c:	movw		r3, #0xd4 
0x10150:	str		r0, [sp, #4] 
0x10154:	movw		r0, #0x630 
0x10158:	movt		r3, #1 
0x1015c:	movt		r0, #1 
0x10160:	mov		lr, pc 
0x10164:	b		#0x10924 
Direct branch: 1, Conditional: 0, Target: 0x10924
**************************************
MB No. 6, Type: 1. Starts at 0x10168 / BB count. 1, Total inst count 6: 
Direct succ: 7 /Remote succ: 0
Indirect succ: 
Direct pred: 13  /Indirect pred: 
0x10168:	ldr		r3, [pc, #0x24] 
0x1016c:	movw		r0, #0x2008 
0x10170:	movt		r0, #2 
0x10174:	sub		r3, r3, r0 
0x10178:	cmp		r3, #6 
0x1017c:	bxls		lr 
Direct branch: 0, Conditional: 1
**************************************
MB No. 7, Type: 1. Starts at 0x10180 / BB count. 1, Total inst count 4: 
Direct succ: 8 /Remote succ: 0
Indirect succ: 
Direct pred: 6  /Indirect pred: 
0x10180:	movw		r3, #0 
0x10184:	movt		r3, #0 
0x10188:	cmp		r3, #0 
0x1018c:	bxeq		lr 
Direct branch: 0, Conditional: 1
**************************************
MB No. 8, Type: 1. Starts at 0x10190 / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 7  /Indirect pred: 
0x10190:	bx		r3 
Direct branch: 0, Conditional: 0
**************************************
MB No. 9, Type: 2. Starts at 0x10198 / BB count. 1, Total inst count 9: 
Direct succ: 10 /Remote succ: 0
Indirect succ: 
Direct pred: 18  /Indirect pred: 
0x10198:	movw		r3, #0x2008 
0x1019c:	movw		r0, #0x2008 
0x101a0:	movt		r3, #2 
0x101a4:	movt		r0, #2 
0x101a8:	sub		r1, r3, r0 
0x101ac:	asr		r1, r1, #2 
0x101b0:	add		r1, r1, r1, lsr #31 
0x101b4:	asrs		r1, r1, #1 
0x101b8:	bxeq		lr 
Direct branch: 0, Conditional: 1
**************************************
MB No. 10, Type: 2. Starts at 0x101bc / BB count. 1, Total inst count 4: 
Direct succ: 11 /Remote succ: 0
Indirect succ: 
Direct pred: 9  /Indirect pred: 
0x101bc:	movw		r3, #0 
0x101c0:	movt		r3, #0 
0x101c4:	cmp		r3, #0 
0x101c8:	bxeq		lr 
Direct branch: 0, Conditional: 1
**************************************
MB No. 11, Type: 2. Starts at 0x101cc / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 10  /Indirect pred: 
0x101cc:	bx		r3 
Direct branch: 0, Conditional: 0
**************************************
MB No. 12, Type: 1. Starts at 0x101d0 / BB count. 1, Total inst count 6: 
Direct succ: 13 /Remote succ: 0
Indirect succ: 
Direct pred:  /Indirect pred: 
0x101d0:	push		{r4, lr} 
0x101d4:	movw		r4, #0x2008 
0x101d8:	movt		r4, #2 
0x101dc:	ldrb		r3, [r4] 
0x101e0:	cmp		r3, #0 
0x101e4:	popne		{r4, pc} 
Direct branch: 0, Conditional: 1
**************************************
MB No. 13, Type: 1. Starts at 0x101e8 / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 6
Indirect succ: 14 
Direct pred: 12  /Indirect pred: 
0x101e8:	bl		#0x10168 
Direct branch: 1, Conditional: 0, Target: 0x10168
**************************************
MB No. 14, Type: 2. Starts at 0x101ec / BB count. 1, Total inst count 4: 
Direct succ: 15 /Remote succ: 15
Indirect succ: 
Direct pred:  /Indirect pred: 
0x101ec:	movw		r3, #0 
0x101f0:	movt		r3, #0 
0x101f4:	cmp		r3, #0 
0x101f8:	beq		#0x10208 
Direct branch: 1, Conditional: 1, Target: 0x10208
**************************************
MB No. 15, Type: 2. Starts at 0x101fc / BB count. 1, Total inst count 6: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 14 14  /Indirect pred: 
0x101fc:	movw		r0, #0x179c 
0x10200:	movt		r0, #1 
0x10204:	nop		 
0x10208:	mov		r3, #1 
0x1020c:	strb		r3, [r4] 
0x10210:	pop		{r4, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 16, Type: 2. Starts at 0x10214 / BB count. 1, Total inst count 5: 
Direct succ: 17 /Remote succ: 17
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10214:	movw		r3, #0 
0x10218:	push		{r4, lr} 
0x1021c:	movt		r3, #0 
0x10220:	cmp		r3, #0 
0x10224:	beq		#0x1023c 
Direct branch: 1, Conditional: 1, Target: 0x1023c
**************************************
MB No. 17, Type: 2. Starts at 0x10228 / BB count. 1, Total inst count 10: 
Direct succ: 18 /Remote succ: 19
Indirect succ: 
Direct pred: 16 16  /Indirect pred: 
0x10228:	movw		r1, #0x200c 
0x1022c:	movw		r0, #0x179c 
0x10230:	movt		r1, #2 
0x10234:	movt		r0, #1 
0x10238:	nop		 
0x1023c:	movw		r0, #0x1ffc 
0x10240:	movt		r0, #2 
0x10244:	ldr		r3, [r0] 
0x10248:	cmp		r3, #0 
0x1024c:	bne		#0x10258 
Direct branch: 1, Conditional: 1, Target: 0x10258
**************************************
MB No. 18, Type: 2. Starts at 0x10250 / BB count. 1, Total inst count 2: 
Direct succ: 0 /Remote succ: 9
Indirect succ: 
Direct pred: 19 21 17  /Indirect pred: 
0x10250:	pop		{r4, lr} 
0x10254:	b		#0x10198 
Direct branch: 1, Conditional: 0, Target: 0x10198
**************************************
MB No. 19, Type: 2. Starts at 0x10258 / BB count. 1, Total inst count 4: 
Direct succ: 20 /Remote succ: 18
Indirect succ: 
Direct pred: 17  /Indirect pred: 
0x10258:	movw		r3, #0 
0x1025c:	movt		r3, #0 
0x10260:	cmp		r3, #0 
0x10264:	beq		#0x10250 
Direct branch: 1, Conditional: 1, Target: 0x10250
**************************************
MB No. 20, Type: 2. Starts at 0x10268 / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 21 
Direct pred: 19  /Indirect pred: 
0x10268:	blx		r3 
Direct branch: 0, Conditional: 0
**************************************
MB No. 21, Type: 2. Starts at 0x1026c / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 18
Indirect succ: 
Direct pred:  /Indirect pred: 
0x1026c:	b		#0x10250 
Direct branch: 1, Conditional: 0, Target: 0x10250
**************************************
MB No. 22, Type: 2. Starts at 0x10270 / BB count. 1, Total inst count 34: 
Direct succ: 23 /Remote succ: 27
Indirect succ: 
Direct pred: 26 36  /Indirect pred: 
0x10270:	push		{fp, lr} 
0x10274:	mov		fp, sp 
0x10278:	sub		sp, sp, #0x24 
0x1027c:	movw		sb, #0x1fec 
0x10280:	movt		sb, #1 
0x10284:	ldr		sb, [pc, sb] 
0x10288:	str		fp, [sb] 
0x1028c:	ldr		ip, [fp, #8] 
0x10290:	movw		lr, #0 
0x10294:	str		fp, [sb] 
0x10298:	str		r0, [fp, #-4] 
0x1029c:	str		fp, [sb] 
0x102a0:	str		r1, [fp, #-8] 
0x102a4:	str		fp, [sb] 
0x102a8:	str		r2, [fp, #-0xc] 
0x102ac:	str		fp, [sb] 
0x102b0:	str		r3, [fp, #-0x10] 
0x102b4:	str		sp, [sb] 
0x102b8:	str		ip, [sp, #0x10] 
0x102bc:	str		sp, [sb] 
0x102c0:	str		lr, [sp, #0xc] 
0x102c4:	movw		sb, #0x1fa4 
0x102c8:	movt		sb, #1 
0x102cc:	ldr		sb, [pc, sb] 
0x102d0:	str		sp, [sb] 
0x102d4:	ldr		r0, [sp, #0xc] 
0x102d8:	str		sp, [sb] 
0x102dc:	ldr		r1, [sp, #0x10] 
0x102e0:	str		fp, [sb] 
0x102e4:	ldr		r2, [fp, #-0x10] 
0x102e8:	sub		r1, r1, r2 
0x102ec:	add		r1, r1, #1 
0x102f0:	cmp		r0, r1 
0x102f4:	bhs		#0x10428 
Direct branch: 1, Conditional: 1, Target: 0x10428
**************************************
MB No. 23, Type: 2. Starts at 0x102f8 / BB count. 1, Total inst count 25: 
Direct succ: 24 /Remote succ: 25
Indirect succ: 
Direct pred: 24 70 79 22  /Indirect pred: 
0x102f8:	movw		sb, #0x1f70 
0x102fc:	movt		sb, #1 
0x10300:	ldr		sb, [pc, sb] 
0x10304:	movw		r0, #0 
0x10308:	vldr		d16, [pc, #0x130] 
0x1030c:	movw		r1, #3 
0x10310:	str		fp, [sb] 
0x10314:	ldr		r2, [fp, #-8] 
0x10318:	str		sp, [sb] 
0x1031c:	ldr		r3, [sp, #0xc] 
0x10320:	add		r2, r2, r3, lsl #3 
0x10324:	vstr		d16, [r2] 
0x10328:	str		sp, [sb] 
0x1032c:	str		r0, [sp, #8] 
0x10330:	str		sp, [sb] 
0x10334:	str		r1, [sp, #4] 
0x10338:	movw		sb, #0x1f30 
0x1033c:	movt		sb, #1 
0x10340:	ldr		sb, [pc, sb] 
0x10344:	str		sp, [sb] 
0x10348:	ldr		r0, [sp, #8] 
0x1034c:	str		fp, [sb] 
0x10350:	ldr		r1, [fp, #-0x10] 
0x10354:	cmp		r0, r1 
0x10358:	bhs		#0x103f4 
Direct branch: 1, Conditional: 1, Target: 0x103f4
**************************************
MB No. 24, Type: 2. Starts at 0x1035c / BB count. 1, Total inst count 38: 
Direct succ: 0 /Remote succ: 23
Indirect succ: 
Direct pred: 23  /Indirect pred: 
0x1035c:	movw		sb, #0x1f0c 
0x10360:	movt		sb, #1 
0x10364:	ldr		sb, [pc, sb] 
0x10368:	movw		r0, #3 
0x1036c:	str		fp, [sb] 
0x10370:	ldr		r1, [fp, #-4] 
0x10374:	str		sp, [sb] 
0x10378:	ldr		r2, [sp, #0xc] 
0x1037c:	str		sp, [sb] 
0x10380:	ldr		r3, [sp, #8] 
0x10384:	add		ip, r2, r3 
0x10388:	add		r1, r1, ip, lsl #3 
0x1038c:	str		r1, [sb] 
0x10390:	vldr		d16, [r1] 
0x10394:	str		fp, [sb] 
0x10398:	ldr		r1, [fp, #-0xc] 
0x1039c:	add		r1, r1, r3, lsl #3 
0x103a0:	str		r1, [sb] 
0x103a4:	vldr		d17, [r1] 
0x103a8:	vmul.f64		d16, d16, d17 
0x103ac:	str		fp, [sb] 
0x103b0:	ldr		r1, [fp, #-8] 
0x103b4:	add		r1, r1, r2, lsl #3 
0x103b8:	str		r1, [sb] 
0x103bc:	vldr		d17, [r1] 
0x103c0:	vadd.f64		d16, d17, d16 
0x103c4:	vstr		d16, [r1] 
0x103c8:	str		sp, [sb] 
0x103cc:	str		r0, [sp] 
0x103d0:	movw		sb, #0x1e98 
0x103d4:	movt		sb, #1 
0x103d8:	ldr		sb, [pc, sb] 
0x103dc:	str		sp, [sb] 
0x103e0:	ldr		r0, [sp, #8] 
0x103e4:	add		r0, r0, #1 
0x103e8:	str		sp, [sb] 
0x103ec:	str		r0, [sp, #8] 
0x103f0:	b		#0x10338 
Direct branch: 1, Conditional: 0, Target: 0x10338
**************************************
MB No. 25, Type: 2. Starts at 0x103f4 / BB count. 1, Total inst count 4: 
Direct succ: 0 /Remote succ: 26
Indirect succ: 
Direct pred: 23  /Indirect pred: 
0x103f4:	movw		sb, #0x1e74 
0x103f8:	movt		sb, #1 
0x103fc:	ldr		sb, [pc, sb] 
0x10400:	b		#0x10404 
Direct branch: 1, Conditional: 0, Target: 0x10404
**************************************
MB No. 26, Type: 2. Starts at 0x10404 / BB count. 1, Total inst count 9: 
Direct succ: 0 /Remote succ: 22
Indirect succ: 
Direct pred: 25  /Indirect pred: 
0x10404:	movw		sb, #0x1e64 
0x10408:	movt		sb, #1 
0x1040c:	ldr		sb, [pc, sb] 
0x10410:	str		sp, [sb] 
0x10414:	ldr		r0, [sp, #0xc] 
0x10418:	add		r0, r0, #1 
0x1041c:	str		sp, [sb] 
0x10420:	str		r0, [sp, #0xc] 
0x10424:	b		#0x102c4 
Direct branch: 1, Conditional: 0, Target: 0x102c4
**************************************
MB No. 27, Type: 2. Starts at 0x10428 / BB count. 1, Total inst count 5: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 22  /Indirect pred: 
0x10428:	movw		sb, #0x1e40 
0x1042c:	movt		sb, #1 
0x10430:	ldr		sb, [pc, sb] 
0x10434:	mov		sp, fp 
0x10438:	pop		{fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 28, Type: 2. Starts at 0x1043c / BB count. 1, Total inst count 11: 
Direct succ: 0 /Remote succ: 89
Indirect succ: 29 
Direct pred:  /Indirect pred: 
0x10444:	andeq		r0, r0, r0 
0x10448:	push		{fp, lr} 
0x1044c:	mov		fp, sp 
0x10450:	sub		sp, sp, #0x28 
0x10454:	movw		sb, #0x1e14 
0x10458:	movt		sb, #1 
0x1045c:	ldr		sb, [pc, sb] 
0x10460:	movw		r0, #0 
0x10464:	bl		#0x10c7c 
Direct branch: 1, Conditional: 0, Target: 0x10c7c
**************************************
MB No. 29, Type: 2. Starts at 0x10468 / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 68
Indirect succ: 30 
Direct pred:  /Indirect pred: 
0x10468:	bl		#0x10b24 
Direct branch: 1, Conditional: 0, Target: 0x10b24
**************************************
MB No. 30, Type: 2. Starts at 0x1046c / BB count. 1, Total inst count 10: 
Direct succ: 31 /Remote succ: 33
Indirect succ: 
Direct pred: 32  /Indirect pred: 
0x1046c:	movw		r0, #0 
0x10470:	str		fp, [sb] 
0x10474:	str		r0, [fp, #-4] 
0x10478:	movw		sb, #0x1df0 
0x1047c:	movt		sb, #1 
0x10480:	ldr		sb, [pc, sb] 
0x10484:	str		fp, [sb] 
0x10488:	ldr		r0, [fp, #-4] 
0x1048c:	cmp		r0, #0xf 
0x10490:	bhs		#0x10534 
Direct branch: 1, Conditional: 1, Target: 0x10534
**************************************
MB No. 31, Type: 2. Starts at 0x10494 / BB count. 1, Total inst count 13: 
Direct succ: 0 /Remote succ: 69
Indirect succ: 32 
Direct pred: 30  /Indirect pred: 
0x10494:	movw		sb, #0x1dd4 
0x10498:	movt		sb, #1 
0x1049c:	ldr		sb, [pc, sb] 
0x104a0:	vldr		d16, [pc, #0x180] 
0x104a4:	movw		r0, #0x20a0 
0x104a8:	movt		r0, #2 
0x104ac:	movw		r1, #3 
0x104b0:	vstr		d16, [fp, #-0x10] 
0x104b4:	str		sp, [sb] 
0x104b8:	str		r0, [sp, #0x14] 
0x104bc:	str		sp, [sb] 
0x104c0:	str		r1, [sp, #0x10] 
0x104c4:	bl		#0x10b3c 
Direct branch: 1, Conditional: 0, Target: 0x10b3c
**************************************
MB No. 32, Type: 2. Starts at 0x104c8 / BB count. 1, Total inst count 27: 
Direct succ: 0 /Remote succ: 30
Indirect succ: 
Direct pred:  /Indirect pred: 
0x104c8:	vmov		s0, r0 
0x104cc:	vcvt.f64.s32		d16, s0 
0x104d0:	vldr		d17, [pc, #0x148] 
0x104d4:	vdiv.f64		d16, d16, d17 
0x104d8:	str		fp, [sb] 
0x104dc:	ldr		r0, [fp, #-4] 
0x104e0:	movw		r1, #0x2028 
0x104e4:	movt		r1, #2 
0x104e8:	add		r0, r1, r0, lsl #3 
0x104ec:	vstr		d16, [r0] 
0x104f0:	str		fp, [sb] 
0x104f4:	ldr		r0, [fp, #-4] 
0x104f8:	movw		r1, #0x20a0 
0x104fc:	movt		r1, #2 
0x10500:	add		r0, r1, r0, lsl #3 
0x10504:	str		fp, [sb] 
0x10508:	vldr		d16, [fp, #-0x10] 
0x1050c:	vstr		d16, [r0] 
0x10510:	movw		sb, #0x1d58 
0x10514:	movt		sb, #1 
0x10518:	ldr		sb, [pc, sb] 
0x1051c:	str		fp, [sb] 
0x10520:	ldr		r0, [fp, #-4] 
0x10524:	add		r0, r0, #1 
0x10528:	str		fp, [sb] 
0x1052c:	str		r0, [fp, #-4] 
0x10530:	b		#0x10478 
Direct branch: 1, Conditional: 0, Target: 0x10478
**************************************
MB No. 33, Type: 2. Starts at 0x10534 / BB count. 1, Total inst count 13: 
Direct succ: 34 /Remote succ: 36
Indirect succ: 
Direct pred: 30 35  /Indirect pred: 
0x10534:	movw		sb, #0x1d34 
0x10538:	movt		sb, #1 
0x1053c:	ldr		sb, [pc, sb] 
0x10540:	movw		r0, #0 
0x10544:	str		fp, [sb] 
0x10548:	str		r0, [fp, #-4] 
0x1054c:	movw		sb, #0x1d1c 
0x10550:	movt		sb, #1 
0x10554:	ldr		sb, [pc, sb] 
0x10558:	str		fp, [sb] 
0x1055c:	ldr		r0, [fp, #-4] 
0x10560:	cmp		r0, #8 
0x10564:	bhs		#0x105e0 
Direct branch: 1, Conditional: 1, Target: 0x105e0
**************************************
MB No. 34, Type: 2. Starts at 0x10568 / BB count. 1, Total inst count 11: 
Direct succ: 0 /Remote succ: 69
Indirect succ: 35 
Direct pred: 33  /Indirect pred: 
0x10568:	movw		sb, #0x1d00 
0x1056c:	movt		sb, #1 
0x10570:	ldr		sb, [pc, sb] 
0x10574:	movw		r0, #0x2118 
0x10578:	movt		r0, #2 
0x1057c:	movw		r1, #3 
0x10580:	str		sp, [sb] 
0x10584:	str		r0, [sp, #0xc] 
0x10588:	str		sp, [sb] 
0x1058c:	str		r1, [sp, #8] 
0x10590:	bl		#0x10b3c 
Direct branch: 1, Conditional: 0, Target: 0x10b3c
**************************************
MB No. 35, Type: 2. Starts at 0x10594 / BB count. 1, Total inst count 19: 
Direct succ: 0 /Remote succ: 33
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10594:	vmov		s0, r0 
0x10598:	vcvt.f64.s32		d16, s0 
0x1059c:	vldr		d17, [pc, #0x7c] 
0x105a0:	vdiv.f64		d16, d16, d17 
0x105a4:	str		fp, [sb] 
0x105a8:	ldr		r0, [fp, #-4] 
0x105ac:	movw		r1, #0x2118 
0x105b0:	movt		r1, #2 
0x105b4:	add		r0, r1, r0, lsl #3 
0x105b8:	vstr		d16, [r0] 
0x105bc:	movw		sb, #0x1cac 
0x105c0:	movt		sb, #1 
0x105c4:	ldr		sb, [pc, sb] 
0x105c8:	str		fp, [sb] 
0x105cc:	ldr		r0, [fp, #-4] 
0x105d0:	add		r0, r0, #1 
0x105d4:	str		fp, [sb] 
0x105d8:	str		r0, [fp, #-4] 
0x105dc:	b		#0x1054c 
Direct branch: 1, Conditional: 0, Target: 0x1054c
**************************************
MB No. 36, Type: 2. Starts at 0x105e0 / BB count. 1, Total inst count 14: 
Direct succ: 0 /Remote succ: 22
Indirect succ: 37 
Direct pred: 33  /Indirect pred: 
0x105e0:	movw		sb, #0x1c88 
0x105e4:	movt		sb, #1 
0x105e8:	ldr		sb, [pc, sb] 
0x105ec:	movw		r0, #0x2028 
0x105f0:	movt		r0, #2 
0x105f4:	movw		r1, #0x20a0 
0x105f8:	movt		r1, #2 
0x105fc:	movw		r2, #0x2118 
0x10600:	movt		r2, #2 
0x10604:	movw		r3, #8 
0x10608:	movw		ip, #0xf 
0x1060c:	str		sp, [sb] 
0x10610:	str		ip, [sp] 
0x10614:	bl		#0x10270 
Direct branch: 1, Conditional: 0, Target: 0x10270
**************************************
MB No. 37, Type: 1. Starts at 0x10618 / BB count. 1, Total inst count 2: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10618:	mov		sp, fp 
0x1061c:	pop		{fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 38, Type: 1. Starts at 0x1087c / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred:  /Indirect pred: 
0x1087c:	ldrshmi		pc, [pc, #0xff] 
Direct branch: 0, Conditional: 1
**************************************
MB No. 39, Type: 1. Starts at 0x108ac / BB count. 1, Total inst count 5: 
Basic Block Id 1, inst count 5
 /  Inst Addr: 0x108ac Inst Addr: 0x108b0 Inst Addr: 0x108b4 Inst Addr: 0x108b8 Inst Addr: 0x108bc
0x108ac:	str		sp, [sb] 
0x108b0:	str		r0, [sp, #8] 
0x108b4:	str		sp, [sb] 
0x108b8:	str		r1, [sp, #4] 
0x108bc:	bl		#0x106a0 
Direct branch: 1, Conditional: 0, Target: 0x106a0
**************************************
MB No. 40, Type: 2. Starts at 0x108c0 / BB count. 1, Total inst count 3: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred:  /Indirect pred: 
0x108c0:	movw		r0, #0 
0x108c4:	mov		sp, fp 
0x108c8:	pop		{fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 41, Type: 2. Starts at 0x108dc / BB count. 1, Total inst count 4: 
Direct succ: 0 /Remote succ: 85
Indirect succ: 42 
Direct pred:  /Indirect pred: 
0x108dc:	movw		r1, #0x1002 
0x108e0:	movt		r0, #1 
0x108e4:	movt		r1, #0x10 
0x108e8:	bl		#0x10c00 
Direct branch: 1, Conditional: 0, Target: 0x10c00
**************************************
MB No. 42, Type: 2. Starts at 0x108ec / BB count. 1, Total inst count 2: 
Direct succ: 43 /Remote succ: 44
Indirect succ: 
Direct pred:  /Indirect pred: 
0x108ec:	cmn		r0, #1 
0x108f0:	beq		#0x10930 
Direct branch: 1, Conditional: 1, Target: 0x10930
**************************************
MB No. 43, Type: 2. Starts at 0x108f4 / BB count. 1, Total inst count 11: 
Direct succ: 0 /Remote succ: 96
Indirect succ: 44 
Direct pred: 42  /Indirect pred: 
0x108f4:	movw		r2, #0 
0x108f8:	mov		r1, #0 
0x108fc:	movt		r2, #0x43c1 
0x10900:	mov		r3, #1 
0x10904:	str		r2, [sp, #8] 
0x10908:	mov		r2, #3 
0x1090c:	str		r1, [sp, #0xc] 
0x10910:	mov		r1, #0x1000 
0x10914:	str		r0, [sp] 
0x10918:	mov		r0, #0 
0x1091c:	bl		#0x10ce4 
Direct branch: 1, Conditional: 0, Target: 0x10ce4
**************************************
MB No. 44, Type: 2. Starts at 0x10920 / BB count. 1, Total inst count 6: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 5 42  /Indirect pred: 
0x10920:	cmn		r0, #1 
0x10924:	movwne		r1, #0x2278 
0x10928:	movtne		r1, #2 
0x1092c:	strne		r0, [r1] 
0x10930:	mov		sp, fp 
0x10934:	pop		{fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 45, Type: 2. Starts at 0x10938 / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10938:	bx		lr 
Direct branch: 0, Conditional: 0
**************************************
MB No. 46, Type: 2. Starts at 0x1093c / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 59  /Indirect pred: 
0x1093c:	bx		lr 
Direct branch: 0, Conditional: 0
**************************************
MB No. 47, Type: 2. Starts at 0x10940 / BB count. 1, Total inst count 10: 
Direct succ: 0 /Remote succ: 108
Indirect succ: 48 
Direct pred: 76  /Indirect pred: 
0x10940:	push		{r4, r5, r6, r7, r8, sl, fp, lr} 
0x10944:	add		fp, sp, #0x18 
0x10948:	sub		sp, sp, #0xb0 
0x1094c:	add		r6, sp, #0x18 
0x10950:	mov		r4, r1 
0x10954:	mov		r5, r0 
0x10958:	mov		r0, r6 
0x1095c:	mov		r1, #0 
0x10960:	mov		r2, #0x98 
0x10964:	bl		#0x10dd8 
Direct branch: 1, Conditional: 0, Target: 0x10dd8
**************************************
MB No. 48, Type: 2. Starts at 0x10968 / BB count. 1, Total inst count 6: 
Direct succ: 49 /Remote succ: 48
Indirect succ: 
Direct pred: 1 48  /Indirect pred: 
0x10968:	movw		r0, #0x2168 
0x1096c:	movt		r0, #2 
0x10970:	str		r5, [r0] 
0x10974:	ldr		r0, [r5], #4 
0x10978:	cmp		r0, #0 
0x1097c:	bne		#0x10974 
Direct branch: 1, Conditional: 1, Target: 0x10974
**************************************
MB No. 49, Type: 2. Starts at 0x10980 / BB count. 1, Total inst count 6: 
Direct succ: 50 /Remote succ: 52
Indirect succ: 
Direct pred: 48  /Indirect pred: 
0x10980:	movw		r8, #0x2280 
0x10984:	movt		r8, #2 
0x10988:	str		r5, [r8, #0x10] 
0x1098c:	ldr		r0, [r5], #4 
0x10990:	cmp		r0, #0 
0x10994:	beq		#0x109c4 
Direct branch: 1, Conditional: 1, Target: 0x109c4
**************************************
MB No. 50, Type: 2. Starts at 0x10998 / BB count. 1, Total inst count 7: 
Direct succ: 51 /Remote succ: 50
Indirect succ: 
Direct pred: 0 2 50 49  /Indirect pred: 
0x10998:	cmp		r0, #0x25 
0x1099c:	ldrls		r1, [r5] 
0x109a0:	strls		r1, [r6, r0, lsl #2] 
0x109a4:	ldr		r0, [r5, #4] 
0x109a8:	add		r5, r5, #8 
0x109ac:	cmp		r0, #0 
0x109b0:	bne		#0x10998 
Direct branch: 1, Conditional: 1, Target: 0x10998
**************************************
MB No. 51, Type: 2. Starts at 0x109b4 / BB count. 1, Total inst count 4: 
Direct succ: 0 /Remote succ: 52
Indirect succ: 
Direct pred: 50  /Indirect pred: 
0x109b4:	ldr		r0, [sp, #0x30] 
0x109b8:	ldr		r2, [sp, #0x58] 
0x109bc:	ldr		r1, [sp, #0x98] 
0x109c0:	b		#0x109d0 
Direct branch: 1, Conditional: 0, Target: 0x109d0
**************************************
MB No. 52, Type: 2. Starts at 0x109c4 / BB count. 1, Total inst count 12: 
Direct succ: 53 /Remote succ: 58
Indirect succ: 
Direct pred: 49 51  /Indirect pred: 
0x109c4:	mov		r0, #0 
0x109c8:	mov		r1, #0 
0x109cc:	mov		r2, #0 
0x109d0:	movw		r3, #0x227c 
0x109d4:	cmp		r4, #0 
0x109d8:	movt		r3, #2 
0x109dc:	str		r0, [r8, #0x24] 
0x109e0:	str		r2, [r3] 
0x109e4:	movw		r2, #0x22c0 
0x109e8:	movt		r2, #2 
0x109ec:	str		r1, [r2] 
0x109f0:	beq		#0x10a38 
Direct branch: 1, Conditional: 1, Target: 0x10a38
**************************************
MB No. 53, Type: 2. Starts at 0x109f4 / BB count. 1, Total inst count 8: 
Direct succ: 0 /Remote succ: 54
Indirect succ: 
Direct pred: 52  /Indirect pred: 
0x109f4:	movw		r0, #0x215c 
0x109f8:	add		r1, r4, #1 
0x109fc:	movt		r0, #2 
0x10a00:	str		r4, [r0] 
0x10a04:	movw		r0, #0x2158 
0x10a08:	movt		r0, #2 
0x10a0c:	str		r4, [r0] 
0x10a10:	b		#0x10a18 
Direct branch: 1, Conditional: 0, Target: 0x10a18
**************************************
MB No. 54, Type: 2. Starts at 0x10a14 / BB count. 1, Total inst count 4: 
Direct succ: 55 /Remote succ: 57
Indirect succ: 
Direct pred: 53 55 57  /Indirect pred: 
0x10a14:	add		r1, r1, #1 
0x10a18:	ldrb		r2, [r1, #-1] 
0x10a1c:	cmp		r2, #0x2f 
0x10a20:	beq		#0x10a30 
Direct branch: 1, Conditional: 1, Target: 0x10a30
**************************************
MB No. 55, Type: 2. Starts at 0x10a24 / BB count. 1, Total inst count 2: 
Direct succ: 56 /Remote succ: 54
Indirect succ: 
Direct pred: 54  /Indirect pred: 
0x10a24:	cmp		r2, #0 
0x10a28:	bne		#0x10a14 
Direct branch: 1, Conditional: 1, Target: 0x10a14
**************************************
MB No. 56, Type: 2. Starts at 0x10a2c / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 58
Indirect succ: 
Direct pred: 55  /Indirect pred: 
0x10a2c:	b		#0x10a38 
Direct branch: 1, Conditional: 0, Target: 0x10a38
**************************************
MB No. 57, Type: 2. Starts at 0x10a30 / BB count. 1, Total inst count 2: 
Direct succ: 0 /Remote succ: 54
Indirect succ: 
Direct pred: 54  /Indirect pred: 
0x10a30:	str		r1, [r0] 
0x10a34:	b		#0x10a14 
Direct branch: 1, Conditional: 0, Target: 0x10a14
**************************************
MB No. 58, Type: 2. Starts at 0x10a38 / BB count. 1, Total inst count 2: 
Direct succ: 0 /Remote succ: 130
Indirect succ: 59 
Direct pred: 52 56  /Indirect pred: 
0x10a38:	add		r0, sp, #0x18 
0x10a3c:	bl		#0x10ff8 
Direct branch: 1, Conditional: 0, Target: 0x10ff8
**************************************
MB No. 59, Type: 2. Starts at 0x10a40 / BB count. 1, Total inst count 2: 
Direct succ: 0 /Remote succ: 46
Indirect succ: 60 
Direct pred:  /Indirect pred: 
0x10a40:	ldr		r0, [sp, #0x7c] 
0x10a44:	bl		#0x1093c 
Direct branch: 1, Conditional: 0, Target: 0x1093c
**************************************
MB No. 60, Type: 2. Starts at 0x10a48 / BB count. 1, Total inst count 4: 
Direct succ: 61 /Remote succ: 63
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10a48:	ldr		r0, [sp, #0x44] 
0x10a4c:	ldr		r1, [sp, #0x48] 
0x10a50:	cmp		r0, r1 
0x10a54:	bne		#0x10a74 
Direct branch: 1, Conditional: 1, Target: 0x10a74
**************************************
MB No. 61, Type: 2. Starts at 0x10a58 / BB count. 1, Total inst count 4: 
Direct succ: 62 /Remote succ: 63
Indirect succ: 
Direct pred: 60  /Indirect pred: 
0x10a58:	ldr		r0, [sp, #0x4c] 
0x10a5c:	ldr		r1, [sp, #0x50] 
0x10a60:	cmp		r0, r1 
0x10a64:	bne		#0x10a74 
Direct branch: 1, Conditional: 1, Target: 0x10a74
**************************************
MB No. 62, Type: 2. Starts at 0x10a68 / BB count. 1, Total inst count 3: 
Direct succ: 63 /Remote succ: 69
Indirect succ: 
Direct pred: 61  /Indirect pred: 
0x10a68:	ldr		r0, [sp, #0x74] 
0x10a6c:	cmp		r0, #0 
0x10a70:	beq		#0x10b3c 
Direct branch: 1, Conditional: 1, Target: 0x10b3c
**************************************
MB No. 63, Type: 2. Starts at 0x10a74 / BB count. 1, Total inst count 15: 
Direct succ: 64 /Remote succ: 65
Indirect succ: 
Direct pred: 60 61 62  /Indirect pred: 
0x10a74:	movw		r0, #0x1750 
0x10a78:	mov		r7, #0xa8 
0x10a7c:	movt		r0, #1 
0x10a80:	vld1.64		{d16, d17}, [r0]! 
0x10a84:	mov		r2, #0 
0x10a88:	vldr		d18, [r0] 
0x10a8c:	mov		r0, sp 
0x10a90:	mov		r1, r0 
0x10a94:	vst1.64		{d16, d17}, [r1]! 
0x10a98:	vstr		d18, [r1] 
0x10a9c:	mov		r1, #3 
0x10aa0:	svc		#0 
0x10aa4:	ldrb		r0, [sp, #6] 
0x10aa8:	tst		r0, #0x20 
0x10aac:	beq		#0x10ad4 
Direct branch: 1, Conditional: 1, Target: 0x10ad4
**************************************
MB No. 64, Type: 2. Starts at 0x10ab0 / BB count. 1, Total inst count 8: 
Direct succ: 65 /Remote succ: 65
Indirect succ: 
Direct pred: 63  /Indirect pred: 
0x10ab0:	movw		r0, #0x1768 
0x10ab4:	movw		r1, #2 
0x10ab8:	movt		r0, #1 
0x10abc:	movt		r1, #2 
0x10ac0:	mov		r7, #5 
0x10ac4:	svc		#0 
0x10ac8:	cmp		r0, #0 
0x10acc:	bge		#0x10ad4 
Direct branch: 1, Conditional: 1, Target: 0x10ad4
**************************************
MB No. 65, Type: 2. Starts at 0x10ad0 / BB count. 1, Total inst count 4: 
Direct succ: 66 /Remote succ: 67
Indirect succ: 
Direct pred: 63 64 64  /Indirect pred: 
0x10ad0:	udf		#0 
0x10ad4:	ldrb		r0, [sp, #0xe] 
0x10ad8:	tst		r0, #0x20 
0x10adc:	beq		#0x10b04 
Direct branch: 1, Conditional: 1, Target: 0x10b04
**************************************
MB No. 66, Type: 2. Starts at 0x10ae0 / BB count. 1, Total inst count 8: 
Direct succ: 67 /Remote succ: 67
Indirect succ: 
Direct pred: 65  /Indirect pred: 
0x10ae0:	movw		r0, #0x1768 
0x10ae4:	movw		r1, #2 
0x10ae8:	movt		r0, #1 
0x10aec:	movt		r1, #2 
0x10af0:	mov		r7, #5 
0x10af4:	svc		#0 
0x10af8:	cmp		r0, #0 
0x10afc:	bge		#0x10b04 
Direct branch: 1, Conditional: 1, Target: 0x10b04
**************************************
MB No. 67, Type: 2. Starts at 0x10b00 / BB count. 1, Total inst count 4: 
Direct succ: 68 /Remote succ: 69
Indirect succ: 
Direct pred: 65 66 66  /Indirect pred: 
0x10b00:	udf		#0 
0x10b04:	ldrb		r0, [sp, #0x16] 
0x10b08:	tst		r0, #0x20 
0x10b0c:	beq		#0x10b34 
Direct branch: 1, Conditional: 1, Target: 0x10b34
**************************************
MB No. 68, Type: 2. Starts at 0x10b10 / BB count. 1, Total inst count 8: 
Direct succ: 69 /Remote succ: 69
Indirect succ: 
Direct pred: 29 67  /Indirect pred: 
0x10b10:	movw		r0, #0x1768 
0x10b14:	movw		r1, #2 
0x10b18:	movt		r0, #1 
0x10b1c:	movt		r1, #2 
0x10b20:	mov		r7, #5 
0x10b24:	svc		#0 
0x10b28:	cmp		r0, #0 
0x10b2c:	bge		#0x10b34 
Direct branch: 1, Conditional: 1, Target: 0x10b34
**************************************
MB No. 69, Type: 2. Starts at 0x10b30 / BB count. 1, Total inst count 5: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 31 34 62 67 68 68  /Indirect pred: 
0x10b30:	udf		#0 
0x10b34:	mov		r0, #1 
0x10b38:	str		r0, [r8, #8] 
0x10b3c:	sub		sp, fp, #0x18 
0x10b40:	pop		{r4, r5, r6, r7, r8, sl, fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 70, Type: 2. Starts at 0x10b44 / BB count. 1, Total inst count 3: 
Direct succ: 0 /Remote succ: 23
Indirect succ: 71 
Direct pred: 77  /Indirect pred: 
0x10b44:	push		{r4, r5, fp, lr} 
0x10b48:	add		fp, sp, #8 
0x10b4c:	bl		#0x1032c 
Direct branch: 1, Conditional: 0, Target: 0x1032c
**************************************
MB No. 71, Type: 2. Starts at 0x10b50 / BB count. 1, Total inst count 6: 
Direct succ: 72 /Remote succ: 74
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10b50:	movw		r4, #0x1ff8 
0x10b54:	movw		r5, #0x1ff4 
0x10b58:	movt		r4, #2 
0x10b5c:	movt		r5, #2 
0x10b60:	cmp		r5, r4 
0x10b64:	bhs		#0x10b78 
Direct branch: 1, Conditional: 1, Target: 0x10b78
**************************************
MB No. 72, Type: 2. Starts at 0x10b68 / BB count. 1, Total inst count 2: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 73 
Direct pred: 73 71  /Indirect pred: 
0x10b68:	ldr		r0, [r5], #4 
0x10b6c:	blx		r0 
Direct branch: 0, Conditional: 0
**************************************
MB No. 73, Type: 2. Starts at 0x10b70 / BB count. 1, Total inst count 2: 
Direct succ: 74 /Remote succ: 72
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10b70:	cmp		r5, r4 
0x10b74:	blo		#0x10b68 
Direct branch: 1, Conditional: 1, Target: 0x10b68
**************************************
MB No. 74, Type: 2. Starts at 0x10b78 / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 71 73  /Indirect pred: 
0x10b78:	pop		{r4, r5, fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 75, Type: 1. Starts at 0x10b7c / BB count. 1, Total inst count 6: 
Basic Block Id 0, inst count 6
 /  Inst Addr: 0x10b7c Inst Addr: 0x10b80 Inst Addr: 0x10b84 Inst Addr: 0x10b88 Inst Addr: 0x10b8c Inst Addr: 0x10b90
0x10b7c:	push		{r4, r5, r6, r7, fp, lr} 
0x10b80:	add		fp, sp, #0x10 
0x10b84:	mov		r4, r2 
0x10b88:	mov		r5, r1 
0x10b8c:	mov		r6, r0 
0x10b90:	bl		#0x108cc 
Direct branch: 1, Conditional: 0, Target: 0x108cc
**************************************
MB No. 76, Type: 2. Starts at 0x10b94 / BB count. 1, Total inst count 5: 
Direct succ: 0 /Remote succ: 47
Indirect succ: 77 
Direct pred:  /Indirect pred: 
0x10b94:	mov		r0, r4 
0x10b98:	ldr		r1, [r0], r5, lsl #2 
0x10b9c:	add		r7, r0, #4 
0x10ba0:	mov		r0, r7 
0x10ba4:	bl		#0x10940 
Direct branch: 1, Conditional: 0, Target: 0x10940
**************************************
MB No. 77, Type: 2. Starts at 0x10ba8 / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 70
Indirect succ: 78 
Direct pred:  /Indirect pred: 
0x10ba8:	bl		#0x10b44 
Direct branch: 1, Conditional: 0, Target: 0x10b44
**************************************
MB No. 78, Type: 2. Starts at 0x10bac / BB count. 1, Total inst count 4: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 79 
Direct pred:  /Indirect pred: 
0x10bac:	mov		r0, r5 
0x10bb0:	mov		r1, r4 
0x10bb4:	mov		r2, r7 
0x10bb8:	blx		r6 
Direct branch: 0, Conditional: 0
**************************************
MB No. 79, Type: 2. Starts at 0x10bbc / BB count. 1, Total inst count 2: 
Direct succ: 0 /Remote succ: 23
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10bbc:	mov		lr, pc 
0x10bc0:	b		#0x10338 
Direct branch: 1, Conditional: 0, Target: 0x10338
**************************************
MB No. 80, Type: 1. Starts at 0x10bc4 / BB count. 1, Total inst count 8: 
Basic Block Id 0, inst count 8
 /  Inst Addr: 0x10bc4 Inst Addr: 0x10bc8 Inst Addr: 0x10bcc Inst Addr: 0x10bd0 Inst Addr: 0x10bd4 Inst Addr: 0x10bd8 Inst Addr: 0x10bdc Inst Addr: 0x10be0
0x10bc4:	push		{r4, r5, fp, lr} 
0x10bc8:	add		fp, sp, #8 
0x10bcc:	movw		r4, #0x1ff8 
0x10bd0:	movw		r5, #0x1ffc 
0x10bd4:	movt		r4, #2 
0x10bd8:	movt		r5, #2 
0x10bdc:	cmp		r5, r4 
0x10be0:	bls		#0x10bf4 / condition: Unsigned lower or same
Direct branch: 1, Conditional: 1, Target: 0x10bf4
**************************************
MB No. 81, Type: 2. Starts at 0x10be4 / BB count. 1, Total inst count 2: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 82 
Direct pred: 82  /Indirect pred: 
0x10be4:	ldr		r0, [r5, #-4]! 
0x10be8:	blx		r0 
Direct branch: 0, Conditional: 0
**************************************
MB No. 82, Type: 1. Starts at 0x10bec / BB count. 1, Total inst count 2: 
Direct succ: 0 /Remote succ: 81
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10bec:	cmp		r5, r4 
0x10bf0:	bhi		#0x10be4 
Direct branch: 1, Conditional: 1, Target: 0x10be4
**************************************
MB No. 83, Type: 1. Starts at 0x10bf4 / BB count. 1, Total inst count 2: 
Basic Block Id 0, inst count 2
 /  Inst Addr: 0x10bf4 Inst Addr: 0x10bf8
0x10bf4:	pop		{r4, r5, fp, lr} 
0x10bf8:	b		#0x1198c 
Direct branch: 1, Conditional: 0, Target: 0x1198c
**************************************
MB No. 84, Type: 2. Starts at 0x10bfc / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10bfc:	bx		lr 
Direct branch: 0, Conditional: 0
**************************************
MB No. 85, Type: 2. Starts at 0x10c00 / BB count. 1, Total inst count 10: 
Direct succ: 86 /Remote succ: 87
Indirect succ: 
Direct pred: 41  /Indirect pred: 
0x10c00:	sub		sp, sp, #8 
0x10c04:	push		{r4, r7, fp, lr} 
0x10c08:	add		fp, sp, #8 
0x10c0c:	sub		sp, sp, #0x10 
0x10c10:	mov		r4, r1 
0x10c14:	mov		r1, r0 
0x10c18:	tst		r4, #0x40 
0x10c1c:	str		r3, [fp, #0xc] 
0x10c20:	str		r2, [fp, #8] 
0x10c24:	bne		#0x10c40 
Direct branch: 1, Conditional: 1, Target: 0x10c40
**************************************
MB No. 86, Type: 2. Starts at 0x10c28 / BB count. 1, Total inst count 6: 
Direct succ: 87 /Remote succ: 87
Indirect succ: 
Direct pred: 85  /Indirect pred: 
0x10c28:	movw		r0, #0x4000 
0x10c2c:	mov		r3, #0 
0x10c30:	movt		r0, #0x40 
0x10c34:	and		r2, r4, r0 
0x10c38:	cmp		r2, r0 
0x10c3c:	bne		#0x10c54 
Direct branch: 1, Conditional: 1, Target: 0x10c54
**************************************
MB No. 87, Type: 2. Starts at 0x10c40 / BB count. 1, Total inst count 12: 
Direct succ: 0 /Remote succ: 118
Indirect succ: 88 
Direct pred: 85 86 86  /Indirect pred: 
0x10c40:	add		r0, fp, #8 
0x10c44:	str		r0, [sp, #0xc] 
0x10c48:	orr		r0, r0, #4 
0x10c4c:	str		r0, [sp, #0xc] 
0x10c50:	ldr		r3, [fp, #8] 
0x10c54:	mov		r0, #0 
0x10c58:	orr		r2, r4, #0x20000 
0x10c5c:	str		r0, [sp] 
0x10c60:	str		r0, [sp, #4] 
0x10c64:	str		r0, [sp, #8] 
0x10c68:	mov		r0, #5 
0x10c6c:	bl		#0x10ed0 
Direct branch: 1, Conditional: 0, Target: 0x10ed0
**************************************
MB No. 88, Type: 2. Starts at 0x10c70 / BB count. 1, Total inst count 3: 
Direct succ: 89 /Remote succ: 90
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10c70:	mov		r3, r0 
0x10c74:	tst		r4, #0x80000 
0x10c78:	beq		#0x10c98 
Direct branch: 1, Conditional: 1, Target: 0x10c98
**************************************
MB No. 89, Type: 2. Starts at 0x10c7c / BB count. 1, Total inst count 2: 
Direct succ: 90 /Remote succ: 90
Indirect succ: 
Direct pred: 28 88  /Indirect pred: 
0x10c7c:	cmp		r3, #0 
0x10c80:	blt		#0x10c98 
Direct branch: 1, Conditional: 1, Target: 0x10c98
**************************************
MB No. 90, Type: 2. Starts at 0x10c84 / BB count. 1, Total inst count 7: 
Direct succ: 0 /Remote succ: 92
Indirect succ: 91 
Direct pred: 88 89 89  /Indirect pred: 
0x10c84:	mov		r7, #0xdd 
0x10c88:	mov		r0, r3 
0x10c8c:	mov		r1, #2 
0x10c90:	mov		r2, #1 
0x10c94:	svc		#0 
0x10c98:	mov		r0, r3 
0x10c9c:	bl		#0x10cb0 
Direct branch: 1, Conditional: 0, Target: 0x10cb0
**************************************
MB No. 91, Type: 2. Starts at 0x10ca0 / BB count. 1, Total inst count 4: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10ca0:	sub		sp, fp, #8 
0x10ca4:	pop		{r4, r7, fp, lr} 
0x10ca8:	add		sp, sp, #8 
0x10cac:	bx		lr 
Direct branch: 0, Conditional: 0
**************************************
MB No. 92, Type: 2. Starts at 0x10cb0 / BB count. 1, Total inst count 4: 
Direct succ: 93 /Remote succ: 0
Indirect succ: 
Direct pred: 90  /Indirect pred: 
0x10cb0:	movw		r1, #0xf001 
0x10cb4:	movt		r1, #0xffff 
0x10cb8:	cmp		r0, r1 
0x10cbc:	bxlo		lr 
Direct branch: 0, Conditional: 1
**************************************
MB No. 93, Type: 2. Starts at 0x10cc0 / BB count. 1, Total inst count 4: 
Direct succ: 0 /Remote succ: 146
Indirect succ: 94 
Direct pred: 92  /Indirect pred: 
0x10cc0:	push		{r4, sl, fp, lr} 
0x10cc4:	add		fp, sp, #8 
0x10cc8:	rsb		r4, r0, #0 
0x10ccc:	bl		#0x111f8 
Direct branch: 1, Conditional: 0, Target: 0x111f8
**************************************
MB No. 94, Type: 2. Starts at 0x10cd0 / BB count. 1, Total inst count 4: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10cd0:	str		r4, [r0] 
0x10cd4:	mvn		r0, #0 
0x10cd8:	pop		{r4, sl, fp, lr} 
0x10cdc:	bx		lr 
Direct branch: 0, Conditional: 0
**************************************
MB No. 95, Type: 2. Starts at 0x10ce0 / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 103  /Indirect pred: 
0x10ce0:	bx		lr 
Direct branch: 0, Conditional: 0
**************************************
MB No. 96, Type: 2. Starts at 0x10ce4 / BB count. 1, Total inst count 12: 
Direct succ: 97 /Remote succ: 99
Indirect succ: 
Direct pred: 43  /Indirect pred: 
0x10ce4:	push		{r4, r5, r6, r7, r8, sl, fp, lr} 
0x10ce8:	add		fp, sp, #0x18 
0x10cec:	ldr		r5, [fp, #0x14] 
0x10cf0:	mov		sl, r0 
0x10cf4:	ldr		r7, [fp, #0x10] 
0x10cf8:	mov		r6, r1 
0x10cfc:	mov		r8, r3 
0x10d00:	lsr		r0, r5, #0xc 
0x10d04:	mov		r1, r7 
0x10d08:	bfi		r1, r0, #0xc, #0x14 
0x10d0c:	cmp		r1, #0 
0x10d10:	beq		#0x10d20 
Direct branch: 1, Conditional: 1, Target: 0x10d20
**************************************
MB No. 97, Type: 2. Starts at 0x10d14 / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 146
Indirect succ: 98 
Direct pred: 96  /Indirect pred: 
0x10d14:	bl		#0x111f8 
Direct branch: 1, Conditional: 0, Target: 0x111f8
**************************************
MB No. 98, Type: 2. Starts at 0x10d18 / BB count. 1, Total inst count 2: 
Direct succ: 0 /Remote succ: 101
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10d18:	mov		r1, #0x16 
0x10d1c:	b		#0x10d30 
Direct branch: 1, Conditional: 0, Target: 0x10d30
**************************************
MB No. 99, Type: 2. Starts at 0x10d20 / BB count. 1, Total inst count 2: 
Direct succ: 100 /Remote succ: 102
Indirect succ: 
Direct pred: 96  /Indirect pred: 
0x10d20:	cmn		r6, #-0x7fffffff 
0x10d24:	blo		#0x10d3c 
Direct branch: 1, Conditional: 1, Target: 0x10d3c
**************************************
MB No. 100, Type: 2. Starts at 0x10d28 / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 146
Indirect succ: 101 
Direct pred: 99  /Indirect pred: 
0x10d28:	bl		#0x111f8 
Direct branch: 1, Conditional: 0, Target: 0x111f8
**************************************
MB No. 101, Type: 2. Starts at 0x10d2c / BB count. 1, Total inst count 4: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 98  /Indirect pred: 
0x10d2c:	mov		r1, #0xc 
0x10d30:	str		r1, [r0] 
0x10d34:	mvn		r0, #0 
0x10d38:	pop		{r4, r5, r6, r7, r8, sl, fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 102, Type: 2. Starts at 0x10d3c / BB count. 1, Total inst count 3: 
Direct succ: 103 /Remote succ: 104
Indirect succ: 
Direct pred: 99  /Indirect pred: 
0x10d3c:	ldr		r4, [fp, #8] 
0x10d40:	tst		r8, #0x10 
0x10d44:	beq		#0x10d58 
Direct branch: 1, Conditional: 1, Target: 0x10d58
**************************************
MB No. 103, Type: 2. Starts at 0x10d48 / BB count. 1, Total inst count 2: 
Direct succ: 0 /Remote succ: 95
Indirect succ: 104 
Direct pred: 102  /Indirect pred: 
0x10d48:	mov		r4, r2 
0x10d4c:	bl		#0x10ce0 
Direct branch: 1, Conditional: 0, Target: 0x10ce0
**************************************
MB No. 104, Type: 2. Starts at 0x10d50 / BB count. 1, Total inst count 10: 
Direct succ: 0 /Remote succ: 149
Indirect succ: 105 
Direct pred: 102  /Indirect pred: 
0x10d50:	mov		r2, r4 
0x10d54:	ldr		r4, [fp, #8] 
0x10d58:	lsr		r0, r7, #0xc 
0x10d5c:	mov		r7, #0xc0 
0x10d60:	mov		r1, r6 
0x10d64:	orr		r5, r0, r5, lsl #20 
0x10d68:	mov		r0, sl 
0x10d6c:	mov		r3, r8 
0x10d70:	svc		#0 
0x10d74:	bl		#0x11250 
Direct branch: 1, Conditional: 0, Target: 0x11250
**************************************
MB No. 105, Type: 2. Starts at 0x10d78 / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10d78:	pop		{r4, r5, r6, r7, r8, sl, fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 106, Type: 2. Starts at 0x10d7c / BB count. 1, Total inst count 6: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10d7c:	movw		r1, #0x2160 
0x10d80:	mov		r3, #0 
0x10d84:	movt		r1, #2 
0x10d88:	sub		r2, r0, #1 
0x10d8c:	strd		r2, r3, [r1] 
0x10d90:	bx		lr 
Direct branch: 0, Conditional: 0
**************************************
MB No. 107, Type: 2. Starts at 0x10d94 / BB count. 1, Total inst count 17: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10d94:	push		{r4, sl, fp, lr} 
0x10d98:	add		fp, sp, #8 
0x10d9c:	movw		ip, #0x2160 
0x10da0:	movw		lr, #0x7f2d 
0x10da4:	movt		ip, #2 
0x10da8:	movt		lr, #0x4c95 
0x10dac:	ldrd		r2, r3, [ip] 
0x10db0:	movw		r4, #0xf42d 
0x10db4:	movt		r4, #0x5851 
0x10db8:	umull		r0, r1, r2, lr 
0x10dbc:	adds		r0, r0, #1 
0x10dc0:	mla		r1, r2, r4, r1 
0x10dc4:	mla		r1, r3, lr, r1 
0x10dc8:	adc		r1, r1, #0 
0x10dcc:	strd		r0, r1, [ip] 
0x10dd0:	lsr		r0, r1, #1 
0x10dd4:	pop		{r4, sl, fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 108, Type: 2. Starts at 0x10dd8 / BB count. 1, Total inst count 4: 
Direct succ: 109 /Remote succ: 116
Indirect succ: 
Direct pred: 47  /Indirect pred: 
0x10dd8:	push		{fp, lr} 
0x10ddc:	mov		fp, sp 
0x10de0:	cmp		r2, #0 
0x10de4:	beq		#0x10ec8 
Direct branch: 1, Conditional: 1, Target: 0x10ec8
**************************************
MB No. 109, Type: 2. Starts at 0x10de8 / BB count. 1, Total inst count 5: 
Direct succ: 110 /Remote succ: 116
Indirect succ: 
Direct pred: 108  /Indirect pred: 
0x10de8:	add		r3, r0, r2 
0x10dec:	cmp		r2, #3 
0x10df0:	strb		r1, [r3, #-1] 
0x10df4:	strb		r1, [r0] 
0x10df8:	blo		#0x10ec8 
Direct branch: 1, Conditional: 1, Target: 0x10ec8
**************************************
MB No. 110, Type: 2. Starts at 0x10dfc / BB count. 1, Total inst count 6: 
Direct succ: 111 /Remote succ: 116
Indirect succ: 
Direct pred: 109  /Indirect pred: 
0x10dfc:	cmp		r2, #7 
0x10e00:	strb		r1, [r3, #-2] 
0x10e04:	strb		r1, [r0, #1] 
0x10e08:	strb		r1, [r3, #-3] 
0x10e0c:	strb		r1, [r0, #2] 
0x10e10:	blo		#0x10ec8 
Direct branch: 1, Conditional: 1, Target: 0x10ec8
**************************************
MB No. 111, Type: 2. Starts at 0x10e14 / BB count. 1, Total inst count 4: 
Direct succ: 112 /Remote succ: 116
Indirect succ: 
Direct pred: 110  /Indirect pred: 
0x10e14:	cmp		r2, #9 
0x10e18:	strb		r1, [r3, #-4] 
0x10e1c:	strb		r1, [r0, #3] 
0x10e20:	blo		#0x10ec8 
Direct branch: 1, Conditional: 1, Target: 0x10ec8
**************************************
MB No. 112, Type: 2. Starts at 0x10e24 / BB count. 1, Total inst count 14: 
Direct succ: 113 /Remote succ: 116
Indirect succ: 
Direct pred: 111  /Indirect pred: 
0x10e24:	movw		r3, #0x101 
0x10e28:	uxtb		r1, r1 
0x10e2c:	movt		r3, #0x101 
0x10e30:	mul		lr, r1, r3 
0x10e34:	rsb		r3, r0, #0 
0x10e38:	and		r1, r3, #3 
0x10e3c:	mov		r3, r0 
0x10e40:	sub		r2, r2, r1 
0x10e44:	bfc		r2, #0, #2 
0x10e48:	str		lr, [r3, r1]! 
0x10e4c:	cmp		r2, #9 
0x10e50:	add		ip, r3, r2 
0x10e54:	str		lr, [ip, #-4] 
0x10e58:	blo		#0x10ec8 
Direct branch: 1, Conditional: 1, Target: 0x10ec8
**************************************
MB No. 113, Type: 2. Starts at 0x10e5c / BB count. 1, Total inst count 6: 
Direct succ: 114 /Remote succ: 116
Indirect succ: 
Direct pred: 112  /Indirect pred: 
0x10e5c:	cmp		r2, #0x19 
0x10e60:	str		lr, [r3, #4] 
0x10e64:	str		lr, [r3, #8] 
0x10e68:	str		lr, [ip, #-0xc] 
0x10e6c:	str		lr, [ip, #-8] 
0x10e70:	blo		#0x10ec8 
Direct branch: 1, Conditional: 1, Target: 0x10ec8
**************************************
MB No. 114, Type: 2. Starts at 0x10e74 / BB count. 1, Total inst count 10: 
Direct succ: 115 /Remote succ: 0
Indirect succ: 
Direct pred: 113  /Indirect pred: 
0x10e74:	vdup.32		q8, lr 
0x10e78:	add		r1, r3, #0xc 
0x10e7c:	vst1.32		{d16, d17}, [r1] 
0x10e80:	sub		r1, ip, #0x1c 
0x10e84:	vst1.32		{d16, d17}, [r1] 
0x10e88:	and		r1, r3, #4 
0x10e8c:	orr		ip, r1, #0x18 
0x10e90:	sub		r2, r2, ip 
0x10e94:	cmp		r2, #0x20 
0x10e98:	poplo		{fp, pc} 
Direct branch: 0, Conditional: 1
**************************************
MB No. 115, Type: 2. Starts at 0x10e9c / BB count. 1, Total inst count 11: 
Direct succ: 116 /Remote succ: 115
Indirect succ: 
Direct pred: 115 114  /Indirect pred: 
0x10e9c:	vmov.32		d16[0], lr 
0x10ea0:	add		r1, r3, ip 
0x10ea4:	vmov.32		d16[1], lr 
0x10ea8:	vorr		d17, d16, d16 
0x10eac:	add		r3, r1, #0x20 
0x10eb0:	vst1.64		{d16, d17}, [r1]! 
0x10eb4:	sub		r2, r2, #0x20 
0x10eb8:	vst1.64		{d16, d17}, [r1] 
0x10ebc:	cmp		r2, #0x1f 
0x10ec0:	mov		r1, r3 
0x10ec4:	bhi		#0x10eac 
Direct branch: 1, Conditional: 1, Target: 0x10eac
**************************************
MB No. 116, Type: 2. Starts at 0x10ec8 / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 108 109 110 111 112 113 115  /Indirect pred: 
0x10ec8:	pop		{fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 117, Type: 2. Starts at 0x10ecc / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 148
Indirect succ: 
Direct pred: 118  /Indirect pred: 
0x10ecc:	b		#0x11228 
Direct branch: 1, Conditional: 0, Target: 0x11228
**************************************
MB No. 118, Type: 2. Starts at 0x10ed0 / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 117
Indirect succ: 
Direct pred: 87  /Indirect pred: 
0x10ed0:	b		#0x10ecc 
Direct branch: 1, Conditional: 0, Target: 0x10ecc
**************************************
MB No. 119, Type: 2. Starts at 0x10ed4 / BB count. 1, Total inst count 7: 
Direct succ: 0 /Remote succ: 177
Indirect succ: 120 
Direct pred:  /Indirect pred: 
0x10ed4:	push		{r4, sl, fp, lr} 
0x10ed8:	add		fp, sp, #8 
0x10edc:	sub		sp, sp, #8 
0x10ee0:	mov		r4, r0 
0x10ee4:	mov		r1, sp 
0x10ee8:	mov		r0, #0 
0x10eec:	bl		#0x11594 
Direct branch: 1, Conditional: 0, Target: 0x11594
**************************************
MB No. 120, Type: 2. Starts at 0x10ef0 / BB count. 1, Total inst count 5: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10ef0:	ldr		r0, [sp] 
0x10ef4:	cmp		r4, #0 
0x10ef8:	strne		r0, [r4] 
0x10efc:	sub		sp, fp, #8 
0x10f00:	pop		{r4, sl, fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 121, Type: 2. Starts at 0x10f04 / BB count. 1, Total inst count 5: 
Direct succ: 0 /Remote succ: 176
Indirect succ: 122 
Direct pred:  /Indirect pred: 
0x10f04:	push		{r4, r7, fp, lr} 
0x10f08:	add		fp, sp, #8 
0x10f0c:	mov		r4, r0 
0x10f10:	str		r4, [r0], #0xa8 
0x10f14:	bl		#0x1157c 
Direct branch: 1, Conditional: 0, Target: 0x1157c
**************************************
MB No. 122, Type: 2. Starts at 0x10f18 / BB count. 1, Total inst count 2: 
Direct succ: 123 /Remote succ: 125
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10f18:	cmp		r0, #0 
0x10f1c:	blt		#0x10f64 
Direct branch: 1, Conditional: 1, Target: 0x10f64
**************************************
MB No. 123, Type: 2. Starts at 0x10f20 / BB count. 1, Total inst count 1: 
Direct succ: 124 /Remote succ: 124
Indirect succ: 
Direct pred: 122  /Indirect pred: 
0x10f20:	bne		#0x10f34 
Direct branch: 1, Conditional: 1, Target: 0x10f34
**************************************
MB No. 124, Type: 2. Starts at 0x10f24 / BB count. 1, Total inst count 16: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 123 123  /Indirect pred: 
0x10f24:	movw		r0, #0x2280 
0x10f28:	mov		r1, #1 
0x10f2c:	movt		r0, #2 
0x10f30:	str		r1, [r0] 
0x10f34:	add		r0, r4, #0x1c 
0x10f38:	mov		r7, #0x100 
0x10f3c:	svc		#0 
0x10f40:	str		r0, [r4, #0x1c] 
0x10f44:	movw		r0, #0x2280 
0x10f48:	movt		r0, #2 
0x10f4c:	add		r0, r0, #0x28 
0x10f50:	str		r0, [r4, #0x78] 
0x10f54:	add		r0, r4, #0x64 
0x10f58:	str		r0, [r4, #0x64] 
0x10f5c:	mov		r0, #0 
0x10f60:	pop		{r4, r7, fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 125, Type: 2. Starts at 0x10f64 / BB count. 1, Total inst count 2: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 122  /Indirect pred: 
0x10f64:	mvn		r0, #0 
0x10f68:	pop		{r4, r7, fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 126, Type: 2. Starts at 0x10f9c / BB count. 1, Total inst count 6: 
Direct succ: 127 /Remote succ: 129
Indirect succ: 
Direct pred: 3  /Indirect pred: 
0x10f9c:	sub		r6, r6, r0 
0x10fa0:	sub		r3, r3, #1 
0x10fa4:	and		r3, r3, r6 
0x10fa8:	cmp		r7, #0 
0x10fac:	add		r4, r0, r3 
0x10fb0:	beq		#0x10fe4 
Direct branch: 1, Conditional: 1, Target: 0x10fe4
**************************************
MB No. 127, Type: 2. Starts at 0x10fb4 / BB count. 1, Total inst count 8: 
Direct succ: 0 /Remote succ: 152
Indirect succ: 128 
Direct pred: 128 126  /Indirect pred: 
0x10fb4:	sub		r1, r2, r1, lsl #2 
0x10fb8:	add		r6, r4, #0xb0 
0x10fbc:	add		r5, r0, r1 
0x10fc0:	ldr		r0, [r7, #0x14] 
0x10fc4:	add		r0, r6, r0 
0x10fc8:	str		r0, [r5], #4 
0x10fcc:	ldmib		r7, {r1, r2} 
0x10fd0:	bl		#0x11280 
Direct branch: 1, Conditional: 0, Target: 0x11280
**************************************
MB No. 128, Type: 2. Starts at 0x10fd4 / BB count. 1, Total inst count 3: 
Direct succ: 129 /Remote succ: 127
Indirect succ: 
Direct pred:  /Indirect pred: 
0x10fd4:	ldr		r7, [r7] 
0x10fd8:	cmp		r7, #0 
0x10fdc:	bne		#0x10fc0 
Direct branch: 1, Conditional: 1, Target: 0x10fc0
**************************************
MB No. 129, Type: 2. Starts at 0x10fe0 / BB count. 1, Total inst count 6: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 126 128  /Indirect pred: 
0x10fe0:	ldr		r1, [r8, #0x20] 
0x10fe4:	mov		r0, r4 
0x10fe8:	str		r1, [sl] 
0x10fec:	str		sl, [r4, #0xac] 
0x10ff0:	str		sl, [r4, #4] 
0x10ff4:	pop		{r4, r5, r6, r7, r8, sl, fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 130, Type: 2. Starts at 0x10ff8 / BB count. 1, Total inst count 5: 
Direct succ: 131 /Remote succ: 137
Indirect succ: 
Direct pred: 58  /Indirect pred: 
0x10ff8:	push		{r4, r5, r6, r7, r8, sl, fp, lr} 
0x10ffc:	add		fp, sp, #0x18 
0x11000:	ldr		r1, [r0, #0x14] 
0x11004:	cmp		r1, #0 
0x11008:	beq		#0x110b4 
Direct branch: 1, Conditional: 1, Target: 0x110b4
**************************************
MB No. 131, Type: 2. Starts at 0x1100c / BB count. 1, Total inst count 11: 
Direct succ: 132 /Remote succ: 134
Indirect succ: 
Direct pred: 134 130  /Indirect pred: 
0x1100c:	ldr		r5, [r0, #0xc] 
0x11010:	movw		ip, #0 
0x11014:	ldr		r6, [r0, #0x10] 
0x11018:	mov		r2, #0 
0x1101c:	movt		ip, #0 
0x11020:	mov		r0, #0 
0x11024:	mov		r3, r5 
0x11028:	ldr		r7, [r3] 
0x1102c:	mov		r4, r5 
0x11030:	cmp		r7, #6 
0x11034:	beq		#0x1104c 
Direct branch: 1, Conditional: 1, Target: 0x1104c
**************************************
MB No. 132, Type: 2. Starts at 0x11038 / BB count. 1, Total inst count 2: 
Direct succ: 133 /Remote succ: 134
Indirect succ: 
Direct pred: 131  /Indirect pred: 
0x11038:	cmp		r7, #2 
0x1103c:	bne		#0x11054 
Direct branch: 1, Conditional: 1, Target: 0x11054
**************************************
MB No. 133, Type: 2. Starts at 0x11040 / BB count. 1, Total inst count 3: 
Direct succ: 134 /Remote succ: 134
Indirect succ: 
Direct pred: 132  /Indirect pred: 
0x11040:	cmp		ip, #0 
0x11044:	mov		r4, ip 
0x11048:	beq		#0x11054 
Direct branch: 1, Conditional: 1, Target: 0x11054
**************************************
MB No. 134, Type: 2. Starts at 0x1104c / BB count. 1, Total inst count 7: 
Direct succ: 135 /Remote succ: 131
Indirect succ: 
Direct pred: 131 132 133 133  /Indirect pred: 
0x1104c:	ldr		r2, [r3, #8] 
0x11050:	sub		r2, r4, r2 
0x11054:	cmp		r7, #7 
0x11058:	moveq		r0, r3 
0x1105c:	add		r3, r3, r6 
0x11060:	subs		r1, r1, #1 
0x11064:	bne		#0x11028 
Direct branch: 1, Conditional: 1, Target: 0x11028
**************************************
MB No. 135, Type: 2. Starts at 0x11068 / BB count. 1, Total inst count 2: 
Direct succ: 136 /Remote succ: 137
Indirect succ: 
Direct pred: 134  /Indirect pred: 
0x11068:	cmp		r0, #0 
0x1106c:	beq		#0x110b4 
Direct branch: 1, Conditional: 1, Target: 0x110b4
**************************************
MB No. 136, Type: 2. Starts at 0x11070 / BB count. 1, Total inst count 17: 
Direct succ: 0 /Remote succ: 137
Indirect succ: 
Direct pred: 135  /Indirect pred: 
0x11070:	ldr		r1, [r0, #8] 
0x11074:	movw		r7, #0x216c 
0x11078:	movt		r7, #2 
0x1107c:	mov		r6, #1 
0x11080:	add		r1, r1, r2 
0x11084:	str		r1, [r7, #4] 
0x11088:	ldr		r2, [r0, #0x10] 
0x1108c:	str		r2, [r7, #8] 
0x11090:	ldr		r3, [r0, #0x14] 
0x11094:	str		r3, [r7, #0xc] 
0x11098:	ldr		r2, [r0, #0x1c] 
0x1109c:	movw		r0, #0x2280 
0x110a0:	movt		r0, #2 
0x110a4:	str		r6, [r0, #0x20] 
0x110a8:	str		r2, [r7, #0x10] 
0x110ac:	str		r7, [r0, #0x14] 
0x110b0:	b		#0x110c8 
Direct branch: 1, Conditional: 0, Target: 0x110c8
**************************************
MB No. 137, Type: 2. Starts at 0x110b4 / BB count. 1, Total inst count 25: 
Direct succ: 138 /Remote succ: 139
Indirect succ: 
Direct pred: 130 135 136  /Indirect pred: 
0x110b4:	movw		r0, #0x216c 
0x110b8:	movt		r0, #2 
0x110bc:	ldr		r1, [r0, #4] 
0x110c0:	ldr		r3, [r0, #0xc] 
0x110c4:	ldr		r2, [r0, #0x10] 
0x110c8:	add		r0, r1, r3 
0x110cc:	sub		r1, r2, #1 
0x110d0:	rsb		r0, r0, #0 
0x110d4:	cmp		r2, #3 
0x110d8:	and		r0, r1, r0 
0x110dc:	movw		r1, #0x216c 
0x110e0:	add		r0, r0, r3 
0x110e4:	movt		r1, #2 
0x110e8:	movls		r2, #4 
0x110ec:	str		r0, [r1, #0xc] 
0x110f0:	add		r0, r2, r0 
0x110f4:	strls		r2, [r1, #0x10] 
0x110f8:	add		r1, r0, #0xbb 
0x110fc:	movw		r8, #0x2280 
0x11100:	bfc		r1, #0, #2 
0x11104:	movt		r8, #2 
0x11108:	cmp		r1, #0xf5 
0x1110c:	str		r1, [r8, #0x18] 
0x11110:	str		r2, [r8, #0x1c] 
0x11114:	blo		#0x11140 
Direct branch: 1, Conditional: 1, Target: 0x11140
**************************************
MB No. 138, Type: 2. Starts at 0x11118 / BB count. 1, Total inst count 10: 
Direct succ: 0 /Remote succ: 139
Indirect succ: 
Direct pred: 137  /Indirect pred: 
0x11118:	mov		r7, #0xc0 
0x1111c:	mov		r0, #0 
0x11120:	mov		r2, #3 
0x11124:	mov		r3, #0x22 
0x11128:	mvn		r4, #0 
0x1112c:	mov		r5, #0 
0x11130:	svc		#0 
0x11134:	ldr		r1, [r8, #0x18] 
0x11138:	ldr		r2, [r8, #0x1c] 
0x1113c:	b		#0x11148 
Direct branch: 1, Conditional: 0, Target: 0x11148
**************************************
MB No. 139, Type: 2. Starts at 0x11140 / BB count. 1, Total inst count 14: 
Direct succ: 140 /Remote succ: 142
Indirect succ: 
Direct pred: 137 138  /Indirect pred: 
0x11140:	movw		r0, #0x2184 
0x11144:	movt		r0, #2 
0x11148:	mvn		r3, #0xaf 
0x1114c:	sub		r2, r2, #1 
0x11150:	sub		r5, r3, r0 
0x11154:	ldr		r3, [r8, #0x20] 
0x11158:	and		r2, r2, r5 
0x1115c:	add		r6, r0, r1 
0x11160:	add		r4, r0, r2 
0x11164:	mvn		r2, r3 
0x11168:	ldr		r7, [r8, #0x14] 
0x1116c:	add		sl, r6, r2, lsl #2 
0x11170:	cmp		r7, #0 
0x11174:	beq		#0x111a8 
Direct branch: 1, Conditional: 1, Target: 0x111a8
**************************************
MB No. 140, Type: 2. Starts at 0x11178 / BB count. 1, Total inst count 8: 
Direct succ: 0 /Remote succ: 152
Indirect succ: 141 
Direct pred: 141 139  /Indirect pred: 
0x11178:	sub		r1, r1, r3, lsl #2 
0x1117c:	add		r6, r4, #0xb0 
0x11180:	add		r5, r0, r1 
0x11184:	ldr		r0, [r7, #0x14] 
0x11188:	add		r0, r6, r0 
0x1118c:	str		r0, [r5], #4 
0x11190:	ldmib		r7, {r1, r2} 
0x11194:	bl		#0x11280 
Direct branch: 1, Conditional: 0, Target: 0x11280
**************************************
MB No. 141, Type: 2. Starts at 0x11198 / BB count. 1, Total inst count 3: 
Direct succ: 142 /Remote succ: 140
Indirect succ: 
Direct pred:  /Indirect pred: 
0x11198:	ldr		r7, [r7] 
0x1119c:	cmp		r7, #0 
0x111a0:	bne		#0x11184 
Direct branch: 1, Conditional: 1, Target: 0x11184
**************************************
MB No. 142, Type: 2. Starts at 0x111a4 / BB count. 1, Total inst count 6: 
Direct succ: 0 /Remote succ: 176
Indirect succ: 143 
Direct pred: 139 141  /Indirect pred: 
0x111a4:	ldr		r3, [r8, #0x20] 
0x111a8:	add		r0, r4, #0xa8 
0x111ac:	str		r3, [sl] 
0x111b0:	str		sl, [r4, #0xac] 
0x111b4:	stm		r4, {r4, sl} 
0x111b8:	bl		#0x1157c 
Direct branch: 1, Conditional: 0, Target: 0x1157c
**************************************
MB No. 143, Type: 2. Starts at 0x111bc / BB count. 1, Total inst count 2: 
Direct succ: 144 /Remote succ: 145
Indirect succ: 
Direct pred:  /Indirect pred: 
0x111bc:	cmp		r0, #0 
0x111c0:	blt		#0x111f0 
Direct branch: 1, Conditional: 1, Target: 0x111f0
**************************************
MB No. 144, Type: 2. Starts at 0x111c4 / BB count. 1, Total inst count 11: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 143  /Indirect pred: 
0x111c4:	moveq		r0, #1 
0x111c8:	mov		r7, #0x100 
0x111cc:	streq		r0, [r8] 
0x111d0:	add		r0, r4, #0x1c 
0x111d4:	svc		#0 
0x111d8:	str		r0, [r4, #0x1c] 
0x111dc:	add		r0, r8, #0x28 
0x111e0:	str		r0, [r4, #0x78] 
0x111e4:	add		r0, r4, #0x64 
0x111e8:	str		r0, [r4, #0x64] 
0x111ec:	pop		{r4, r5, r6, r7, r8, sl, fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 145, Type: 2. Starts at 0x111f0 / BB count. 1, Total inst count 2: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 143  /Indirect pred: 
0x111f0:	udf		#0 
0x111f4:	pop		{r4, r5, r6, r7, r8, sl, fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 146, Type: 2. Starts at 0x111f8 / BB count. 1, Total inst count 3: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 93 97 100 150  /Indirect pred: 
0x111f8:	mrc		p15, #0, r0, c13, c0, #3 
0x111fc:	sub		r0, r0, #0x80 
0x11200:	bx		lr 
Direct branch: 0, Conditional: 0
**************************************
MB No. 147, Type: 2. Starts at 0x11204 / BB count. 1, Total inst count 9: 
Direct succ: 0 /Remote succ: 147
Indirect succ: 
Direct pred: 147  /Indirect pred: 
0x11204:	push		{r7, sl, fp, lr} 
0x11208:	add		fp, sp, #8 
0x1120c:	mov		r7, #0xf8 
0x11210:	mov		r1, r0 
0x11214:	svc		#0 
0x11218:	mov		r7, #1 
0x1121c:	mov		r0, r1 
0x11220:	svc		#0 
0x11224:	b		#0x1121c 
Direct branch: 1, Conditional: 0, Target: 0x1121c
**************************************
MB No. 148, Type: 2. Starts at 0x11228 / BB count. 1, Total inst count 10: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 117  /Indirect pred: 
0x11228:	mov		ip, sp 
0x1122c:	push		{r4, r5, r6, r7} 
0x11230:	mov		r7, r0 
0x11234:	mov		r0, r1 
0x11238:	mov		r1, r2 
0x1123c:	mov		r2, r3 
0x11240:	ldm		ip, {r3, r4, r5, r6} 
0x11244:	svc		#0 
0x11248:	pop		{r4, r5, r6, r7} 
0x1124c:	bx		lr 
Direct branch: 0, Conditional: 0
**************************************
MB No. 149, Type: 2. Starts at 0x11250 / BB count. 1, Total inst count 4: 
Direct succ: 150 /Remote succ: 0
Indirect succ: 
Direct pred: 104 181 184  /Indirect pred: 
0x11250:	movw		r1, #0xf001 
0x11254:	movt		r1, #0xffff 
0x11258:	cmp		r0, r1 
0x1125c:	bxlo		lr 
Direct branch: 0, Conditional: 1
**************************************
MB No. 150, Type: 2. Starts at 0x11260 / BB count. 1, Total inst count 4: 
Direct succ: 0 /Remote succ: 146
Indirect succ: 151 
Direct pred: 149  /Indirect pred: 
0x11260:	push		{r4, sl, fp, lr} 
0x11264:	add		fp, sp, #8 
0x11268:	rsb		r4, r0, #0 
0x1126c:	bl		#0x111f8 
Direct branch: 1, Conditional: 0, Target: 0x111f8
**************************************
MB No. 151, Type: 2. Starts at 0x11270 / BB count. 1, Total inst count 4: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred:  /Indirect pred: 
0x11270:	str		r4, [r0] 
0x11274:	mvn		r0, #0 
0x11278:	pop		{r4, sl, fp, lr} 
0x1127c:	bx		lr 
Direct branch: 0, Conditional: 0
**************************************
MB No. 152, Type: 2. Starts at 0x11280 / BB count. 1, Total inst count 4: 
Direct succ: 153 /Remote succ: 175
Indirect succ: 
Direct pred: 127 140  /Indirect pred: 
0x11280:	push		{r0, r4, lr} 
0x11284:	sub		sp, sp, #0x1c 
0x11288:	cmp		r2, #4 
0x1128c:	blo		#0x11554 
Direct branch: 1, Conditional: 1, Target: 0x11554
**************************************
MB No. 153, Type: 2. Starts at 0x11290 / BB count. 1, Total inst count 3: 
Direct succ: 154 /Remote succ: 154
Indirect succ: 
Direct pred: 152  /Indirect pred: 
0x11290:	rsb		r3, r1, #0 
0x11294:	ands		r3, r3, #3 
0x11298:	beq		#0x112bc 
Direct branch: 1, Conditional: 1, Target: 0x112bc
**************************************
MB No. 154, Type: 2. Starts at 0x1129c / BB count. 1, Total inst count 11: 
Direct succ: 155 /Remote succ: 160
Indirect succ: 
Direct pred: 153 153  /Indirect pred: 
0x1129c:	lsls		ip, r3, #0x1f 
0x112a0:	sub		r2, r2, r3 
0x112a4:	ldrbmi		r3, [r1], #1 
0x112a8:	ldrbhs		r4, [r1], #1 
0x112ac:	ldrbhs		ip, [r1], #1 
0x112b0:	strbmi		r3, [r0], #1 
0x112b4:	strbhs		r4, [r0], #1 
0x112b8:	strbhs		ip, [r0], #1 
0x112bc:	eor		ip, r0, r1 
0x112c0:	tst		ip, #3 
0x112c4:	bne		#0x11368 
Direct branch: 1, Conditional: 1, Target: 0x11368
**************************************
MB No. 155, Type: 2. Starts at 0x112c8 / BB count. 1, Total inst count 4: 
Direct succ: 156 /Remote succ: 156
Indirect succ: 
Direct pred: 154  /Indirect pred: 
0x112c8:	stm		sp, {r5, r6, r7, r8, sb, sl, fp} 
0x112cc:	rsb		r3, r0, #0 
0x112d0:	ands		r3, r3, #0x1c 
0x112d4:	beq		#0x11304 
Direct branch: 1, Conditional: 1, Target: 0x11304
**************************************
MB No. 156, Type: 2. Starts at 0x112d8 / BB count. 1, Total inst count 13: 
Direct succ: 157 /Remote succ: 158
Indirect succ: 
Direct pred: 155 155  /Indirect pred: 
0x112d8:	cmp		r3, r2 
0x112dc:	andhi		r3, r2, #0x1c 
0x112e0:	lsls		ip, r3, #0x1c 
0x112e4:	ldmhs		r1!, {r4, r5, r6, r7} 
0x112e8:	ldmmi		r1!, {r8, sb} 
0x112ec:	stmhs		r0!, {r4, r5, r6, r7} 
0x112f0:	stmmi		r0!, {r8, sb} 
0x112f4:	tst		r3, #4 
0x112f8:	ldrne		sl, [r1], #4 
0x112fc:	strne		sl, [r0], #4 
0x11300:	sub		r2, r2, r3 
0x11304:	subs		r2, r2, #0x20 
0x11308:	blo		#0x11320 
Direct branch: 1, Conditional: 1, Target: 0x11320
**************************************
MB No. 157, Type: 2. Starts at 0x1130c / BB count. 1, Total inst count 4: 
Direct succ: 158 /Remote succ: 157
Indirect succ: 
Direct pred: 157 156  /Indirect pred: 
0x1130c:	ldm		r1!, {r4, r5, r6, r7, r8, sb, sl, fp} 
0x11310:	subs		r2, r2, #0x20 
0x11314:	stm		r0!, {r4, r5, r6, r7, r8, sb, sl, fp} 
0x11318:	bhs		#0x1130c 
Direct branch: 1, Conditional: 1, Target: 0x1130c
**************************************
MB No. 158, Type: 2. Starts at 0x1131c / BB count. 1, Total inst count 3: 
Direct succ: 159 /Remote succ: 159
Indirect succ: 
Direct pred: 156 157  /Indirect pred: 
0x1131c:	add		r2, r2, #0x20 
0x11320:	tst		r2, #0x1f 
0x11324:	beq		#0x1135c 
Direct branch: 1, Conditional: 1, Target: 0x1135c
**************************************
MB No. 159, Type: 2. Starts at 0x11328 / BB count. 1, Total inst count 16: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 158 158  /Indirect pred: 
0x11328:	lsls		ip, r2, #0x1c 
0x1132c:	ldmhs		r1!, {r4, r5, r6, r7} 
0x11330:	ldmmi		r1!, {r8, sb} 
0x11334:	stmhs		r0!, {r4, r5, r6, r7} 
0x11338:	stmmi		r0!, {r8, sb} 
0x1133c:	lsls		ip, r2, #0x1e 
0x11340:	ldrhs		r3, [r1], #4 
0x11344:	ldrhmi		r4, [r1], #2 
0x11348:	strhs		r3, [r0], #4 
0x1134c:	strhmi		r4, [r0], #2 
0x11350:	tst		r2, #1 
0x11354:	ldrbne		r3, [r1] 
0x11358:	strbne		r3, [r0] 
0x1135c:	pop		{r5, r6, r7, r8, sb, sl, fp} 
0x11360:	pop		{r0, r4, lr} 
0x11364:	bx		lr 
Direct branch: 0, Conditional: 0
**************************************
MB No. 160, Type: 2. Starts at 0x11368 / BB count. 1, Total inst count 2: 
Direct succ: 161 /Remote succ: 175
Indirect succ: 
Direct pred: 154  /Indirect pred: 
0x11368:	cmp		r2, #4 
0x1136c:	blo		#0x11554 
Direct branch: 1, Conditional: 1, Target: 0x11554
**************************************
MB No. 161, Type: 2. Starts at 0x11370 / BB count. 1, Total inst count 16: 
Direct succ: 162 /Remote succ: 175
Indirect succ: 
Direct pred: 160  /Indirect pred: 
0x11370:	stm		sp, {r5, r6, r7, r8, sb, sl, fp} 
0x11374:	rsb		r5, r0, #0 
0x11378:	and		r5, r5, #3 
0x1137c:	lsl		ip, r5, #3 
0x11380:	rsb		lr, ip, #0x20 
0x11384:	ldr		r3, [r1], #4 
0x11388:	sub		r2, r2, #4 
0x1138c:	lsls		r5, r5, #0x1f 
0x11390:	strbmi		r3, [r0], #1 
0x11394:	lsrmi		r3, r3, #8 
0x11398:	strbhs		r3, [r0], #1 
0x1139c:	lsrhs		r3, r3, #8 
0x113a0:	strbhs		r3, [r0], #1 
0x113a4:	lsrhs		r3, r3, #8 
0x113a8:	cmp		r2, #4 
0x113ac:	blo		#0x11538 
Direct branch: 1, Conditional: 1, Target: 0x11538
**************************************
MB No. 162, Type: 2. Starts at 0x113b0 / BB count. 1, Total inst count 2: 
Direct succ: 163 /Remote succ: 165
Indirect succ: 
Direct pred: 163 161  /Indirect pred: 
0x113b0:	tst		r0, #0x1c 
0x113b4:	beq		#0x113d8 
Direct branch: 1, Conditional: 1, Target: 0x113d8
**************************************
MB No. 163, Type: 2. Starts at 0x113b8 / BB count. 1, Total inst count 7: 
Direct succ: 164 /Remote succ: 162
Indirect succ: 
Direct pred: 162  /Indirect pred: 
0x113b8:	ldr		r5, [r1], #4 
0x113bc:	sub		r2, r2, #4 
0x113c0:	orr		r4, r3, r5, lsl lr 
0x113c4:	lsr		r3, r5, ip 
0x113c8:	str		r4, [r0], #4 
0x113cc:	cmp		r2, #4 
0x113d0:	bhs		#0x113b0 
Direct branch: 1, Conditional: 1, Target: 0x113b0
**************************************
MB No. 164, Type: 2. Starts at 0x113d4 / BB count. 1, Total inst count 1: 
Direct succ: 165 /Remote succ: 175
Indirect succ: 
Direct pred: 163  /Indirect pred: 
0x113d4:	blo		#0x11538 
Direct branch: 1, Conditional: 1, Target: 0x11538
**************************************
MB No. 165, Type: 2. Starts at 0x113d8 / BB count. 1, Total inst count 2: 
Direct succ: 166 /Remote succ: 173
Indirect succ: 
Direct pred: 162 164  /Indirect pred: 
0x113d8:	subs		r2, r2, #0x20 
0x113dc:	blo		#0x1150c 
Direct branch: 1, Conditional: 1, Target: 0x1150c
**************************************
MB No. 166, Type: 2. Starts at 0x113e0 / BB count. 1, Total inst count 2: 
Direct succ: 167 /Remote succ: 172
Indirect succ: 
Direct pred: 165  /Indirect pred: 
0x113e0:	cmp		ip, #0x18 
0x113e4:	beq		#0x114b0 
Direct branch: 1, Conditional: 1, Target: 0x114b0
**************************************
MB No. 167, Type: 2. Starts at 0x113e8 / BB count. 1, Total inst count 2: 
Direct succ: 168 /Remote succ: 170
Indirect succ: 
Direct pred: 166  /Indirect pred: 
0x113e8:	cmp		ip, #8 
0x113ec:	beq		#0x11450 
Direct branch: 1, Conditional: 1, Target: 0x11450
**************************************
MB No. 168, Type: 2. Starts at 0x113f0 / BB count. 1, Total inst count 23: 
Direct succ: 169 /Remote succ: 168
Indirect succ: 
Direct pred: 168 167  /Indirect pred: 
0x113f0:	ldr		ip, [r1], #4 
0x113f4:	mov		r4, ip 
0x113f8:	ldm		r1!, {r5, r6, r7, r8, sb, sl, fp} 
0x113fc:	subs		r2, r2, #0x20 
0x11400:	ldrhs		ip, [r1], #4 
0x11404:	orr		r3, r3, r4, lsl #16 
0x11408:	lsr		r4, r4, #0x10 
0x1140c:	orr		r4, r4, r5, lsl #16 
0x11410:	lsr		r5, r5, #0x10 
0x11414:	orr		r5, r5, r6, lsl #16 
0x11418:	lsr		r6, r6, #0x10 
0x1141c:	orr		r6, r6, r7, lsl #16 
0x11420:	lsr		r7, r7, #0x10 
0x11424:	orr		r7, r7, r8, lsl #16 
0x11428:	lsr		r8, r8, #0x10 
0x1142c:	orr		r8, r8, sb, lsl #16 
0x11430:	lsr		sb, sb, #0x10 
0x11434:	orr		sb, sb, sl, lsl #16 
0x11438:	lsr		sl, sl, #0x10 
0x1143c:	orr		sl, sl, fp, lsl #16 
0x11440:	stm		r0!, {r3, r4, r5, r6, r7, r8, sb, sl} 
0x11444:	lsr		r3, fp, #0x10 
0x11448:	bhs		#0x113f4 
Direct branch: 1, Conditional: 1, Target: 0x113f4
**************************************
MB No. 169, Type: 2. Starts at 0x1144c / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 173
Indirect succ: 
Direct pred: 168  /Indirect pred: 
0x1144c:	b		#0x1150c 
Direct branch: 1, Conditional: 0, Target: 0x1150c
**************************************
MB No. 170, Type: 2. Starts at 0x11450 / BB count. 1, Total inst count 23: 
Direct succ: 171 /Remote succ: 170
Indirect succ: 
Direct pred: 167 170  /Indirect pred: 
0x11450:	ldr		ip, [r1], #4 
0x11454:	mov		r4, ip 
0x11458:	ldm		r1!, {r5, r6, r7, r8, sb, sl, fp} 
0x1145c:	subs		r2, r2, #0x20 
0x11460:	ldrhs		ip, [r1], #4 
0x11464:	orr		r3, r3, r4, lsl #24 
0x11468:	lsr		r4, r4, #8 
0x1146c:	orr		r4, r4, r5, lsl #24 
0x11470:	lsr		r5, r5, #8 
0x11474:	orr		r5, r5, r6, lsl #24 
0x11478:	lsr		r6, r6, #8 
0x1147c:	orr		r6, r6, r7, lsl #24 
0x11480:	lsr		r7, r7, #8 
0x11484:	orr		r7, r7, r8, lsl #24 
0x11488:	lsr		r8, r8, #8 
0x1148c:	orr		r8, r8, sb, lsl #24 
0x11490:	lsr		sb, sb, #8 
0x11494:	orr		sb, sb, sl, lsl #24 
0x11498:	lsr		sl, sl, #8 
0x1149c:	orr		sl, sl, fp, lsl #24 
0x114a0:	stm		r0!, {r3, r4, r5, r6, r7, r8, sb, sl} 
0x114a4:	lsr		r3, fp, #8 
0x114a8:	bhs		#0x11454 
Direct branch: 1, Conditional: 1, Target: 0x11454
**************************************
MB No. 171, Type: 2. Starts at 0x114ac / BB count. 1, Total inst count 1: 
Direct succ: 0 /Remote succ: 173
Indirect succ: 
Direct pred: 170  /Indirect pred: 
0x114ac:	b		#0x1150c 
Direct branch: 1, Conditional: 0, Target: 0x1150c
**************************************
MB No. 172, Type: 2. Starts at 0x114b0 / BB count. 1, Total inst count 23: 
Direct succ: 173 /Remote succ: 172
Indirect succ: 
Direct pred: 166 172  /Indirect pred: 
0x114b0:	ldr		ip, [r1], #4 
0x114b4:	mov		r4, ip 
0x114b8:	ldm		r1!, {r5, r6, r7, r8, sb, sl, fp} 
0x114bc:	subs		r2, r2, #0x20 
0x114c0:	ldrhs		ip, [r1], #4 
0x114c4:	orr		r3, r3, r4, lsl #8 
0x114c8:	lsr		r4, r4, #0x18 
0x114cc:	orr		r4, r4, r5, lsl #8 
0x114d0:	lsr		r5, r5, #0x18 
0x114d4:	orr		r5, r5, r6, lsl #8 
0x114d8:	lsr		r6, r6, #0x18 
0x114dc:	orr		r6, r6, r7, lsl #8 
0x114e0:	lsr		r7, r7, #0x18 
0x114e4:	orr		r7, r7, r8, lsl #8 
0x114e8:	lsr		r8, r8, #0x18 
0x114ec:	orr		r8, r8, sb, lsl #8 
0x114f0:	lsr		sb, sb, #0x18 
0x114f4:	orr		sb, sb, sl, lsl #8 
0x114f8:	lsr		sl, sl, #0x18 
0x114fc:	orr		sl, sl, fp, lsl #8 
0x11500:	stm		r0!, {r3, r4, r5, r6, r7, r8, sb, sl} 
0x11504:	lsr		r3, fp, #0x18 
0x11508:	bhs		#0x114b4 
Direct branch: 1, Conditional: 1, Target: 0x114b4
**************************************
MB No. 173, Type: 2. Starts at 0x1150c / BB count. 1, Total inst count 4: 
Direct succ: 174 /Remote succ: 175
Indirect succ: 
Direct pred: 165 169 171 172  /Indirect pred: 
0x1150c:	rsb		ip, lr, #0x20 
0x11510:	add		r2, r2, #0x20 
0x11514:	cmp		r2, #4 
0x11518:	blo		#0x11538 
Direct branch: 1, Conditional: 1, Target: 0x11538
**************************************
MB No. 174, Type: 2. Starts at 0x1151c / BB count. 1, Total inst count 7: 
Direct succ: 175 /Remote succ: 174
Indirect succ: 
Direct pred: 174 173  /Indirect pred: 
0x1151c:	ldr		r5, [r1], #4 
0x11520:	sub		r2, r2, #4 
0x11524:	orr		r4, r3, r5, lsl lr 
0x11528:	lsr		r3, r5, ip 
0x1152c:	str		r4, [r0], #4 
0x11530:	cmp		r2, #4 
0x11534:	bhs		#0x1151c 
Direct branch: 1, Conditional: 1, Target: 0x1151c
**************************************
MB No. 175, Type: 2. Starts at 0x11538 / BB count. 1, Total inst count 17: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 152 160 161 164 173 174  /Indirect pred: 
0x11538:	lsls		r5, lr, #0x1c 
0x1153c:	strbmi		r3, [r0], #1 
0x11540:	lsrmi		r3, r3, #8 
0x11544:	strbhs		r3, [r0], #1 
0x11548:	lsrhs		r3, r3, #8 
0x1154c:	strbhs		r3, [r0], #1 
0x11550:	ldm		sp, {r5, r6, r7, r8, sb, sl, fp} 
0x11554:	lsls		r2, r2, #0x1f 
0x11558:	ldrbmi		r2, [r1], #1 
0x1155c:	ldrbhs		r3, [r1], #1 
0x11560:	ldrbhs		ip, [r1] 
0x11564:	strbmi		r2, [r0], #1 
0x11568:	strbhs		r3, [r0], #1 
0x1156c:	strbhs		ip, [r0] 
0x11570:	add		sp, sp, #0x1c 
0x11574:	pop		{r0, r4, lr} 
0x11578:	bx		lr 
Direct branch: 0, Conditional: 0
**************************************
MB No. 176, Type: 2. Starts at 0x1157c / BB count. 1, Total inst count 6: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 121 142  /Indirect pred: 
0x1157c:	push		{r7, sl, fp, lr} 
0x11580:	add		fp, sp, #8 
0x11584:	movw		r7, #5 
0x11588:	movt		r7, #0xf 
0x1158c:	svc		#0 
0x11590:	pop		{r7, sl, fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 177, Type: 2. Starts at 0x11594 / BB count. 1, Total inst count 9: 
Direct succ: 178 /Remote succ: 182
Indirect succ: 
Direct pred: 119  /Indirect pred: 
0x11594:	push		{r4, r5, r7, sl, fp, lr} 
0x11598:	add		fp, sp, #0x10 
0x1159c:	mov		r5, r0 
0x115a0:	movw		r0, #0x2004 
0x115a4:	movt		r0, #2 
0x115a8:	mov		r4, r1 
0x115ac:	ldr		r2, [r0] 
0x115b0:	cmp		r2, #0 
0x115b4:	beq		#0x115e0 
Direct branch: 1, Conditional: 1, Target: 0x115e0
**************************************
MB No. 178, Type: 2. Starts at 0x115b8 / BB count. 1, Total inst count 3: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 179 
Direct pred: 177  /Indirect pred: 
0x115b8:	mov		r0, r5 
0x115bc:	mov		r1, r4 
0x115c0:	blx		r2 
Direct branch: 0, Conditional: 0
**************************************
MB No. 179, Type: 2. Starts at 0x115c4 / BB count. 1, Total inst count 2: 
Direct succ: 180 /Remote succ: 185
Indirect succ: 
Direct pred:  /Indirect pred: 
0x115c4:	cmp		r0, #0 
0x115c8:	beq		#0x1162c 
Direct branch: 1, Conditional: 1, Target: 0x1162c
**************************************
MB No. 180, Type: 2. Starts at 0x115cc / BB count. 1, Total inst count 2: 
Direct succ: 181 /Remote succ: 182
Indirect succ: 
Direct pred: 179  /Indirect pred: 
0x115cc:	cmn		r0, #0x16 
0x115d0:	bne		#0x115e0 
Direct branch: 1, Conditional: 1, Target: 0x115e0
**************************************
MB No. 181, Type: 2. Starts at 0x115d4 / BB count. 1, Total inst count 3: 
Direct succ: 0 /Remote succ: 149
Indirect succ: 
Direct pred: 180  /Indirect pred: 
0x115d4:	mvn		r0, #0x15 
0x115d8:	pop		{r4, r5, r7, sl, fp, lr} 
0x115dc:	b		#0x11250 
Direct branch: 1, Conditional: 0, Target: 0x11250
**************************************
MB No. 182, Type: 2. Starts at 0x115e0 / BB count. 1, Total inst count 6: 
Direct succ: 183 /Remote succ: 184
Indirect succ: 
Direct pred: 177 180  /Indirect pred: 
0x115e0:	movw		r7, #0x107 
0x115e4:	mov		r0, r5 
0x115e8:	mov		r1, r4 
0x115ec:	svc		#0 
0x115f0:	cmn		r0, #0x26 
0x115f4:	bne		#0x11604 
Direct branch: 1, Conditional: 1, Target: 0x11604
**************************************
MB No. 183, Type: 2. Starts at 0x115f8 / BB count. 1, Total inst count 3: 
Direct succ: 184 /Remote succ: 185
Indirect succ: 
Direct pred: 182  /Indirect pred: 
0x115f8:	mvn		r0, #0x15 
0x115fc:	cmp		r5, #0 
0x11600:	beq		#0x1160c 
Direct branch: 1, Conditional: 1, Target: 0x1160c
**************************************
MB No. 184, Type: 2. Starts at 0x11604 / BB count. 1, Total inst count 2: 
Direct succ: 0 /Remote succ: 149
Indirect succ: 
Direct pred: 182 183  /Indirect pred: 
0x11604:	pop		{r4, r5, r7, sl, fp, lr} 
0x11608:	b		#0x11250 
Direct branch: 1, Conditional: 0, Target: 0x11250
**************************************
MB No. 185, Type: 2. Starts at 0x1160c / BB count. 1, Total inst count 10: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 179 183  /Indirect pred: 
0x1160c:	mov		r7, #0x4e 
0x11610:	mov		r0, r4 
0x11614:	mov		r1, #0 
0x11618:	svc		#0 
0x1161c:	mov		r1, #0x3e8 
0x11620:	ldr		r0, [r4, #4] 
0x11624:	mul		r0, r0, r1 
0x11628:	str		r0, [r4, #4] 
0x1162c:	mov		r0, #0 
0x11630:	pop		{r4, r5, r7, sl, fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 186, Type: 1. Starts at 0x11634 / BB count. 1, Total inst count 9: 
Basic Block Id 0, inst count 9
 /  Inst Addr: 0x11634 Inst Addr: 0x11638 Inst Addr: 0x1163c Inst Addr: 0x11640 Inst Addr: 0x11644 Inst Addr: 0x11648 Inst Addr: 0x1164c Inst Addr: 0x11650 Inst Addr: 0x11654
0x11634:	push		{r4, r5, fp, lr} 
0x11638:	add		fp, sp, #8 
0x1163c:	mov		r4, r1 
0x11640:	movw		r1, #0x177c 
0x11644:	mov		r5, r0 
0x11648:	movw		r0, #0x1772 
0x1164c:	movt		r0, #1 
0x11650:	movt		r1, #1 
0x11654:	bl		#0x116ac 
Direct branch: 1, Conditional: 0, Target: 0x116ac
**************************************
MB No. 187, Type: 2. Starts at 0x11658 / BB count. 1, Total inst count 9: 
Direct succ: 188 /Remote succ: 189
Indirect succ: 
Direct pred: 188  /Indirect pred: 
0x11658:	movw		r1, #0x13dc 
0x1165c:	mov		r2, r0 
0x11660:	movw		r0, #0x2004 
0x11664:	movt		r1, #1 
0x11668:	movt		r0, #2 
0x1166c:	dmb		ish 
0x11670:	ldrex		r3, [r0] 
0x11674:	cmp		r3, r1 
0x11678:	bne		#0x11688 
Direct branch: 1, Conditional: 1, Target: 0x11688
**************************************
MB No. 188, Type: 2. Starts at 0x1167c / BB count. 1, Total inst count 3: 
Direct succ: 189 /Remote succ: 187
Indirect succ: 
Direct pred: 187  /Indirect pred: 
0x1167c:	strex		r3, r2, [r0] 
0x11680:	cmp		r3, #0 
0x11684:	bne		#0x11670 
Direct branch: 1, Conditional: 1, Target: 0x11670
**************************************
MB No. 189, Type: 2. Starts at 0x11688 / BB count. 1, Total inst count 3: 
Direct succ: 190 /Remote succ: 191
Indirect succ: 
Direct pred: 187 188  /Indirect pred: 
0x11688:	cmp		r2, #0 
0x1168c:	dmb		ish 
0x11690:	beq		#0x116a4 
Direct branch: 1, Conditional: 1, Target: 0x116a4
**************************************
MB No. 190, Type: 2. Starts at 0x11694 / BB count. 1, Total inst count 4: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 189  /Indirect pred: 
0x11694:	mov		r0, r5 
0x11698:	mov		r1, r4 
0x1169c:	pop		{r4, r5, fp, lr} 
0x116a0:	bx		r2 
Direct branch: 0, Conditional: 0
**************************************
MB No. 191, Type: 2. Starts at 0x116a4 / BB count. 1, Total inst count 2: 
Direct succ: 0 /Remote succ: 0
Indirect succ: 
Direct pred: 189  /Indirect pred: 
0x116a4:	mvn		r0, #0x25 
0x116a8:	pop		{r4, r5, fp, pc} 
Direct branch: 0, Conditional: 0
**************************************
MB No. 192, Type: 1. Starts at 0x116ac / BB count. 1, Total inst count 10: 
Basic Block Id 0, inst count 10
 /  Inst Addr: 0x116ac Inst Addr: 0x116b0 Inst Addr: 0x116b4 Inst Addr: 0x116b8 Inst Addr: 0x116bc Inst Addr: 0x116c0 Inst Addr: 0x116c4 Inst Addr: 0x116c8 Inst Addr: 0x116cc Inst Addr: 0x116d0
0x116ac:	push		{r4, r5, r6, r7, r8, sl, fp, lr} 
0x116b0:	add		fp, sp, #0x18 
0x116b4:	sub		sp, sp, #0x18 
0x116b8:	str		r0, [sp, #0x14] 
0x116bc:	movw		r0, #0x2280 
0x116c0:	movt		r0, #2 
0x116c4:	mov		ip, r1 
0x116c8:	ldr		r0, [r0, #0x10] 
0x116cc:	add		r0, r0, #4 
0x116d0:	b		#0x116d8 
Direct branch: 1, Conditional: 0, Target: 0x116d8
**************************************
MB No. 193, Type: 1. Starts at 0x116d4 / BB count. 1, Total inst count 4: 
Basic Block Id 0, inst count 4
 /  Inst Addr: 0x116d4 Inst Addr: 0x116d8 Inst Addr: 0x116dc Inst Addr: 0x116e0
0x116d4:	add		r0, r0, #8 
0x116d8:	ldr		r1, [r0, #-4] 
0x116dc:	cmp		r1, #0 
0x116e0:	beq		#0x11930 / condition: Equal
Direct branch: 1, Conditional: 1, Target: 0x11930
**************************************
MB No. 194, Type: 1. Starts at 0x116e4 / BB count. 1, Total inst count 2: 
Basic Block Id 0, inst count 2
 /  Inst Addr: 0x116e4 Inst Addr: 0x116e8
0x116e4:	cmp		r1, #0x21 
0x116e8:	bne		#0x116d4 / condition: Not equal
Direct branch: 1, Conditional: 1, Target: 0x116d4
**************************************
MB No. 195, Type: 1. Starts at 0x116ec / BB count. 1, Total inst count 4: 
Basic Block Id 0, inst count 4
 /  Inst Addr: 0x116ec Inst Addr: 0x116f0 Inst Addr: 0x116f4 Inst Addr: 0x116f8
0x116ec:	ldr		r1, [r0] 
0x116f0:	mov		r0, #0 
0x116f4:	cmp		r1, #0 
0x116f8:	beq		#0x11934 / condition: Equal
Direct branch: 1, Conditional: 1, Target: 0x11934
**************************************
MB No. 196, Type: 1. Starts at 0x116fc / BB count. 1, Total inst count 3: 
Basic Block Id 0, inst count 3
 /  Inst Addr: 0x116fc Inst Addr: 0x11700 Inst Addr: 0x11704
0x116fc:	ldrh		r3, [r1, #0x2c] 
0x11700:	cmp		r3, #0 
0x11704:	beq		#0x11934 / condition: Equal
Direct branch: 1, Conditional: 1, Target: 0x11934
**************************************
MB No. 197, Type: 1. Starts at 0x11708 / BB count. 1, Total inst count 10: 
Basic Block Id 0, inst count 10
 /  Inst Addr: 0x11708 Inst Addr: 0x1170c Inst Addr: 0x11710 Inst Addr: 0x11714 Inst Addr: 0x11718 Inst Addr: 0x1171c Inst Addr: 0x11720 Inst Addr: 0x11724 Inst Addr: 0x11728 Inst Addr: 0x1172c
0x11708:	ldr		r2, [r1, #0x1c] 
0x1170c:	mvn		r6, #0 
0x11710:	ldrh		r0, [r1, #0x2a] 
0x11714:	mov		r5, #0 
0x11718:	add		r2, r1, r2 
0x1171c:	add		r7, r2, #4 
0x11720:	mov		r2, #0 
0x11724:	ldr		r4, [r7, #-4] 
0x11728:	cmp		r4, #2 
0x1172c:	beq		#0x1174c / condition: Equal
Direct branch: 1, Conditional: 1, Target: 0x1174c
