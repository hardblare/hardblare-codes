/* test1.c */

/**  
REMARKS DIFT Coprocessor v1: 
The program generates dependencies (also called annotations) for Microblaze coprocessor. There are three types of instructions: 
	i. TAG INITIALIZATION => Annotations that end with 0x2
		* I1) tag(reg) = tag => register value is specified in bits 31 to 28. The tag value is specified in bit 27. The bits 26 to 15 are available to be used for tags. This annotation ends with 0x12.
		* I2)tag(mem) = tag => To initialize a memory address at a given tag value, this instruction can be used. The bit 27 is used for tag value. Bits 31 to 28 and 26 to 15 are available. This type of 			     annotation ends with 0x22. The memory value is contained in the next annotation. 
		* I3) tag(offset) = tag =>  Similar to tag(mem) = tag but ends with 0x42.
	ii. TAG UPDATE => Annotations that end with 0x4
		* U1) tag(reg_dst) = tag(reg_src + offset) Annotation that ends with 0x84 => Offset sign can be found by looking in bit 15.
		* U2) tag(mem_address) = tag(register) Annotation that ends with 0x104 + another Annotation that contains memory address
		* U3) tag(register) = tag(mem_address) Annotation that ends with 0x204 + another Annotation that contains memory address
		* U4) tag(reg_dst) = tag(reg_src1) + tag(reg_src2) Annotation that ends with 0x404
		* U5) tag(reg_dst) = tag(reg_src1)  + tag (reg_src2 + offset) 
		* U6) PUSH/POP => Annotation that ends with 0x9004 (PUSH) or 0x1004 (POP) => The registers that need to be pushed or popped are stored in bits 31 to 16. On the Microblaze, just right shift these bits to get the registers to save on the stack. It means that this annotation has the following effects on the coprocessor side: the registers are pushed on the stack or popped. => We will need push/pop instructions for our coprocessor as well. 
		* U7) tag(reg_dst + offset) = tag(reg_src) => Annotation that ends with 0x2004. 
	iii. TAG CHECK => Annotations that end with 0x1. 

The program looks for instructions given in CODE; (The CODE is generated using bin2elf function found in ~/Documents/Capstone/Capstone/tests/essais/)
For each instruction, the type of each operands is looked for and saved in op_dest, op_src1, op_src2 and op_src3. 
Then, the switch(ops) generates annotations for each case. Consider if ops is 0x11(reg <=> 1 cf op_type enum), it means that the destination operand is a register and the first operand is also a register. Then it generates the annotation according to the above-mentionned formats. It belongs to the type U1 => So, the printed annotation contains the destintation register number, the operand register number and the type U1 (annotation ends with 0x84)...
**/
#include <stdio.h>
#include <inttypes.h>
#include <string.h>  
#include <capstone/capstone.h>

#define CODE ""

#define TAILLE 		10000

typedef enum op_type{
	unknown	= 0,
	reg, // 1
	imm, // 2
	mem, // 3

} op_type_t;

// OPCODES Custom DIFT coprocessor v2
/*
* tagi means tag initialization operation
* tagu means tag update operation
* tagc means tag check operation
* r means register, imm means immediate and m means memory
* i means
* "*_s" means propagation rule specified by security policy
* "*_o" means propagation rule specified by opcode
*/
#define tagi_r_imm 		0x20
#define tagi_r_r 		0x21
#define tagi_m_r 		0x22
#define tagu_rrr_s		0x23
#define tagu_m_r_s		0x24
#define tagu_r_m_s 		0x25
#define tagu_rrr_o 		0x26
#define tag_inst_r 		0x27 // for ARM STR instructions
#define tag_r_inst 		0x28 // for ARM LDR instructions
#define tag_k_ps2pl		0x29
#define tag_k_pl2ps		0x2a
#define tag_check_o 		0x2b

// Function for rrr_o 
#define FUNC_ADD 		0x0
#define FUNC_SUB		0x1
#define FUNC_OR			0x2
#define FUNC_AND		0x3
#define FUNC_XOR		0x4
#define FUNC_NOR		0x5
#define FUNC_COPY_SRC1		0x6
#define FUNC_COPY_SRC2		0x7

long absolute(long value) {
  if (value < 0) {
    return -value;
  }
  else {
    return value;  
  }
}

#define signed_hex_char(x) 	( ((x)<0)?"-":"" )
#define signed_hex(x) 	   	( ((x)<0)?-(unsigned)(x):(x) )
#define sign(x)			( ((x)<0)?(1):0 )
#define tag_constants		(0)
#define PUSH			"push"
#define POP			"pop"

//#define DEBUG_ON 		1

/** @TODO: How to recognize if an instruction is an instruction or an address **/

int main(int argc, char **argv){
	csh handle;
	cs_insn *insn;
	size_t count;
	unsigned n;
	static unsigned counter;	
	unsigned op_dest[TAILLE] = {0}, op_src1[TAILLE] = {0}, op_src2[TAILLE] = {0}, op_src3[TAILLE] = {0};
	int operands_push[TAILLE];
	int operands_pop[TAILLE];
	unsigned instructions_idx_push[TAILLE]={0};
	unsigned instructions_idx_pop[TAILLE]={0};
	int temp = 0;
	unsigned ldr[TAILLE] = {2};
	static unsigned count_id_ldr;
	int val_dest[TAILLE] = {0}, val_src1[TAILLE] = {0}, val_src2[TAILLE] = {0}, val_src3[TAILLE] = {0}, temp_val = 0;
	int instruction_address; 
	if (cs_open(CS_ARCH_ARM, CS_MODE_ARM, &handle) != CS_ERR_OK)
		return -1;
	cs_option(handle, CS_OPT_DETAIL, CS_OPT_ON); // turn ON detail feature with CS_OPT_ON
	cs_option(handle, CS_OPT_SKIPDATA, CS_OPT_ON); // 
	long start_address; 
	char *ptr;
	//cs_detail *detail;
	if (argc == 2)
		start_address = strtoul(argv[1],&ptr,16);
	else {
		printf("usage: The application requires an argument that gives the start address of the application\n %s 0x9fc4\n", argv[0]);
		start_address = 0x0;
		return -2;		
	}

	count = cs_disasm(handle, CODE, sizeof(CODE)-1, start_address, 0, &insn);
	static unsigned push_counter, pop_counter, branch_counter;
	unsigned temporary_idx = 0; 
	static unsigned count_ldr, count_instrumentation_ldr; 
	if (count > 0) {
		size_t j;
		for (j = 0; j < count; j++) {
			counter++;
//			//printf("%x\n",counter);	
			cs_insn *i = &(insn[j]);
			if (cs_insn_name(handle, i->id) != NULL){
				cs_detail *detail = i->detail;
				instruction_address = i->address;
	#ifdef DEBUG_ON
				printf("0x%"PRIx64":\t%s\t\t%s\n", i->address, i->mnemonic,i->op_str);
				printf("PC= %"PRIx64", 0x%"PRIx64":\t%s\t\t%s // insn-mnem: %s\n",(i->address + 0x8), i->address, i->mnemonic, i->op_str, cs_insn_name(handle, i->id));
	#endif
				if ( strcmp(i->mnemonic, "b") == 0 || strcmp(i->mnemonic, "beq") == 0 || strcmp(i->mnemonic, "bne") == 0 || strncmp(i->mnemonic, "bx", 2) == 0 || strcmp(i->mnemonic, "bls") == 0 || strncmp(i->mnemonic, "bl", 2) == 0){
					branch_counter++;
					//printf("\t\tNumber of branches %u\n", branch_counter);		
				}
				if (strncmp(i->mnemonic, "ldm", 3) == 0 || strncmp(i->mnemonic, "stm", 3) == 0)
					count_instrumentation_ldr++, count_ldr++;

	/*
				if (detail->regs_read_count > 0) {
					//printf("\tImplicit registers read: ");
					for (n = 0; n < detail->regs_read_count; n++) {
						//printf("%s ", cs_reg_name(handle, detail->regs_read[n]));
					}
					//printf("\n");
				} 
	*/
				/*else
					printf("No implicit reg read");*/
							// print implicit registers modified by this instruction
	/*			if (detail->regs_write_count > 0) {
					//printf("\tImplicit registers modified: ");
					for (n = 0; n < detail->regs_write_count; n++) {
						//printf("%s ", cs_reg_name(handle, detail->regs_write[n]));
					}
					//printf("\n");
				}			
	*/
				/*if (detail->groups_count > 0) {
					printf("\tThis instruction belongs to groups: ");
					for (n = 0; n < detail->groups_count; n++) {
					    	printf("%u ", detail->groups[n]);
					}
					printf("\n");
				} 
				else
					printf("No group");*/
			
				if (strncmp(i->mnemonic,PUSH,3) == 0) {// PUSH instruction
					push_counter++;
				}
				else if (strncmp(i->mnemonic,POP,3) == 0) {// POP instruction
					pop_counter++;
				}
				temp = 0; temp_val = 0;
				for (n = 0; n < detail->arm.op_count; n++) {	//op_count gives the number of operands for an instruction
					cs_arm_op *op = &(detail->arm.operands[n]);				
					ldr[j] = 2;
	//				//printf("OP TYPE : %x <- ---\n", op->type);
					switch(op->type) {
						case ARM_OP_REG:
							//printf("\t\toperands[%u].type: REG = %s %d %x\n", n, cs_reg_name(handle,op->reg), op->reg, op->reg);
							temp = reg;
						
							if (op->reg == 10) temp_val = 14;// lr
							else if (op->reg == 11) temp_val = 15;// pc
							else if (op->reg == 12) temp_val = 13;// sp
							else if (op->reg == 77) temp_val = 11;// fp
							else temp_val = (op->reg-0x42);
						
							if (strncmp(i->mnemonic,PUSH,3) == 0) {// PUSH instruction
								operands_push[push_counter] |= (1<<temp_val);							
	#ifdef DEBUG_ON
								printf("\t\t\tOperands PUSH %08x, %08x\n", (1 << temp_val), operands_push[push_counter]);
	#endif
								instructions_idx_push[n] = j; // store index					
								temp = 0xF0000000;
								temp_val = 0x0;
							}
							else if (strncmp(i->mnemonic,POP, 3) == 0 ) {// POP instruction
								operands_pop[pop_counter] |= (1<<temp_val); 
	#ifdef DEBUG_ON
								printf("\t\t\tOperands POP %08x, %x \n", (1<<temp_val), operands_pop[pop_counter]);
	#endif
								instructions_idx_pop[n] = j; // store index					
								temp = 0xE0000000;
								temp_val = 0x0;
							}
							break;
						case ARM_OP_IMM:
	#ifdef DEBUG_ON
						       printf("\t\toperands[%u].type: IMM = 0x%x\n", n, op->imm);
	#endif
							temp = imm;
							temp_val = op->imm;
						       break;
					     case ARM_OP_FP:
	#ifdef DEBUG_ON
						       printf("\t\toperands[%u].type: FP = %f\n", n, op->fp);
	#endif
						       temp = unknown; 
							temp_val = 0;
						       break;
					     case ARM_OP_MEM: /** @TODO: Deal with instructions that need to be instrumented **/					    
						    count_instrumentation_ldr++;
						    // only integer operations considered in this work
						    if (strncmp(i->mnemonic, "ldr",3) == 0) {
						    	ldr[j] = 1;
						    	count_id_ldr++;
						    	//printf("MEM instruction ldr = %d, %s\n",ldr[j], cs_insn_name(handle, i->id));
						    }
						    // else if (strncmp(i->mnemonic, "vldr",4) == 0) {
						    // 	ldr = 1; 
						    // 	//printf("MEM instruction ldr = %d, %s\n",ldr, cs_insn_name(handle, i->id));
						    // }
						    else if (strncmp(i->mnemonic, "str",3) == 0){
						    	ldr[j] = 0;
						    	//printf("MEM instruction ldr = %d, %s\n",ldr[j], cs_insn_name(handle, i->id));
						    }
						    // else if (strncmp(i->mnemonic, "vstr",4) == 0){
						    // 	ldr = 0;
						    // 	//printf("MEM instruction ldr = %d, %s\n",ldr, cs_insn_name(handle, i->id));
						    // }
					    	else{
					    		ldr[j] = 2;
					    		//printf("MEM instruction ldr = %d, %s\n",ldr[j], cs_insn_name(handle, i->id));
					    	}
							//printf("count instrumentation ldr %x\n",count_instrumentation_ldr);
							temp = mem;
							temp_val = 0;
						       //printf("\t\toperands[%u].type: MEM Base = %d, index = %d, disp = %d\n", n, op->mem.base, op->mem.index, op->mem.disp);
							if (op->mem.base == 12) // SP-relative address 									 
							 	temp_val = (13)<<24 | (absolute(op->mem.disp))<<16 | sign(op->mem.disp)<<15;
							else if (op->mem.base == 11){ // PC-relative address									
							 	temp_val = (15)<<24 | (absolute(op->mem.disp))<<16 | sign(op->mem.disp)<<15;
							}
							//else if (op->mem.base = (77) )
								//temp_val = (11)<<24 | (absolute(op->mem.disp))<<16 | sign(op->mem.disp)<<15;
							else{
								if (op->mem.index == 0)
									temp_val = (op->mem.base-0x42)<<24 | (op->mem.disp)<<16 | sign(op->mem.disp)<<15 ;
								else
									temp_val = (op->mem.base-0x42)<<24 | (op->mem.disp)<<16 | sign(op->mem.disp)<<15 | (op->mem.index-0x42)<<8;
								if (strncmp(i->mnemonic, "strb",4) == 0 || strncmp(i->mnemonic, "ldrb",4) == 0 || strncmp(i->mnemonic, "ldrd", 4) == 0 || strncmp(i->mnemonic, "strd", 4) == 0 || strncmp(i->mnemonic, "ldm", 3) == 0 || strncmp(i->mnemonic, "stm", 3) == 0 || strncmp(i->mnemonic, "vldr",4) == 0 || strncmp(i->mnemonic, "vstr",4) == 0 || strncmp(i->mnemonic,"vldm",4) == 0 || strncmp(i->mnemonic, "vstm",4) == 0)
		                                        		count_ldr++;//,printf("count_ldr = 0x%x\n", count_ldr),temp_val = (op->mem.base-0x42)<<24 | (op->mem.disp)<<16 | sign(op->mem.disp)<<15;
							}

							/*if (strncmp(i->mnemonic, "strb",4) == 0 || strncmp(i->mnemonic, "ldrb",4) == 0 || strncmp(i->mnemonic, "ldrd", 4) == 0 || strncmp(i->mnemonic, "strd", 4) == 0 || strncmp(i->mnemonic, "ldm", 3) == 0 || strncmp(i->mnemonic, "stm", 3) == 0)
									                                                count_ldr++,printf("count_ldr = 0x%x\n", count_ldr),temp_val = (op->mem.base-0x42)<<24 | (op->mem.disp)<<16 | sign(op->mem.disp)<<15;*/
						       if (op->mem.base != ARM_REG_INVALID)
							 //printf("\t\t\toperands[%u].mem.base: REG = %s\n", n, cs_reg_name(handle, op->mem.base));
						       if (op->mem.index != ARM_REG_INVALID)
							 //printf("\t\t\toperands[%u].mem.index: REG = %s\n", n, cs_reg_name(handle, op->mem.index));
						       if (op->mem.disp != 0)
							 //printf("\t\t\toperands[%u].mem.disp: %s 0x%x\n", n,signed_hex_char(op->mem.disp),signed_hex(op->mem.disp));						

							////printf("\t\t\t\top->mem.base -0x42= 0x%x, op->mem.disp = %x, op->mem.index = %x, sign(op->mem.disp)<<15 = 0x%x, %x, temp_val = %d\n", op->mem.base-0x42, op->mem.disp, op->mem.index, sign(op->mem.disp)<<15, (((op->mem.base)-0x42)<<24 | (op->mem.disp)<<16  ), temp_val);
						       break;
					     case ARM_OP_CIMM:
						       //printf("\t\toperands[%u].type: C-IMM = %u\n", n, op->imm);
						       temp = unknown; 
							temp_val = 0;
						       break;
					     default: 
						       temp = unknown; 
							temp_val = 0;
						       break;
					}
					switch(n){
						case 0:
							op_dest[j] = temp;
							val_dest[j] = temp_val;
							break;
						case 1:
							op_src1[j] = temp;
							val_src1[j] = temp_val;
							break;
						case 2:
							op_src2[j] = temp;
							val_src2[j] = temp_val;
							break;
						case 3:
							op_src3[j] = temp;
							val_src3[j] = temp_val;
							break;
						default:					
							break;
					}
				}
	#ifdef DEBUG_ON
	 			if (detail->arm.op_count == 1){
	 				printf("Information flow value(type) : %x(%x) <- ---\n", val_dest[j], op_dest[j]);
	 			}
	 			if (detail->arm.op_count == 2)	
	 				printf("Information flow value(type) : %x(%x) <- %x(%x)\n", val_dest[j], op_dest[j], val_src1[j], op_src1[j]);
	 			if (detail->arm.op_count == 3)	
	 				printf("Information flow value(type) : %x(%x) <- %x(%x), %x(%x)\n", val_dest[j], op_dest[j], val_src1[j], op_src1[j], val_src2[j], op_src2[j]);
	 			if (detail->arm.op_count == 4)	
	 				printf("Information flow value(type) : %x(%x) <- %x(%x), %x(%x), %x(%x)\n", val_dest[j], op_dest[j], val_src1[j], op_src1[j], val_src2[j], op_src2[j], val_src3[j], op_src3[j]);
	//			printf("Information flow value(type) : %x(%x) <- %x(%x), %x(%x), %x(%x)\n", val_dest[j], op_dest[j], val_src1[j], op_src1[j], val_src2[j], op_src2[j], val_src3[j], op_src3[j]);

	#endif

				cs_arm *arm;
				arm = &(i->detail->arm);
				if (arm->cc != ARM_CC_AL && arm->cc != ARM_CC_INVALID) {
		        		//printf("\tCode condition: %u\n", arm->cc);
			}
	
	    		if (arm->update_flags) {
			        //printf("\tUpdate-flags: True\n");
			}

			if (arm->writeback) {
			        //printf("\tWrite-back: True\n");
			}
		}
	}
	cs_free(insn, count);
	} else
		printf("ERROR: Failed to disassemble given code!\n");

#ifdef DEBUG_ON
	printf("2nd loop\n");
#endif
	size_t j;
	static unsigned push_counter_write, pop_counter_write, annotation;
	unsigned temp_index, push_annotations, pop_annotations;
	count = cs_disasm(handle, CODE, sizeof(CODE)-1, start_address, 0, &insn);	
	if (count > 0){
	for (j = 0; j < count; j++) {
		cs_insn *i = &(insn[j]);			
		cs_detail *detail = i->detail;
		instruction_address = i->address;
		printf("0x%x: \t", (unsigned)instruction_address); // important to run script
#ifdef DEBUG_ON 
		printf("insn-mnem %s\t", i->mnemonic);
		printf("0x%x: \t", (unsigned)instruction_address);
		printf("PC= %lx\n",(i->address + 0x8));
#endif
		unsigned int ops = (op_dest[j]) | (op_src1[j])<<4 | (op_src2[j])<<8 | (op_src3[j])<<12 ;
		//printf("ops = %x\n", ops);
#ifdef DEBUG_ON
		printf("ops = %x\n", ops);
#endif
		const unsigned int reg_reg = 17;//(1 | 1<<4);
		const unsigned int reg_imm = 33;//(1 | 2<<4); 

		const unsigned int reg_mem = 49;//(1 | 3<<4);		
//		for (n = 0; n < detail->arm.op_count; n++) {
//			cs_arm_op *op = &(detail->arm.operands[n]);
		switch(ops){
			default:
#ifdef DEBUG_ON
				printf("PC= %x, 0x%"PRIx64":\t%s\t\t%s // insn-mnem: %s\n",(i->address + 0x8), i->address, i->mnemonic, i->op_str, cs_insn_name(handle, i->id));
				//printf("ops = %08x\n", ops);
#endif
				break;
			case 0 | 1<<4 | 1<<8 | 1<<12 | 1<<16:
#ifdef DEBUG_ON
				printf("PC= %x, 0x%"PRIx64":\t%s\t\t%s // insn-mnem: %s\n",(i->address + 0x8), i->address, i->mnemonic, i->op_str, cs_insn_name(handle, i->id));	
				//printf("ops = %08x\n", ops);
#endif
				annotation = 0x0;
				printf("%08x", annotation);
				break;
			case 0x11: // dst = reg, src = reg => tag(dst) = tag(src) (=> MOV instruction e.g. ...) => Tag update (U1)
				/*** FIXED PUSH, POP Instructions that have 2 operands ... AND ALSO FIX CMP INSTRUCTIONS... ***/
				/*** Require a general purpose instruction as well to update SP (for push, pop or ldr, str instructions .... ***/
				//@DONE: FIXED: @ 0x103ec, no dependencies generated ??? (Memory initalization problem
#ifdef DEBUG_ON
				printf("PC= %x, 0x%"PRIx64":\t%s\t\t%s // insn-mnem: %s\n",(i->address + 0x8), i->address, i->mnemonic, i->op_str, cs_insn_name(handle, i->id));
				//printf("ops = %08x\n", ops);
#endif				
				annotation = ( val_dest[j]==val_src1[j] ) ? 0 : (tagi_r_r << 26 |(val_dest[j])<<21|(val_src1[j])<<16);

				printf("%08x", annotation);
				break;
			case 0x21: // dst = reg, src = imm => tag(dst) = tag_constants (=> MOV instruction e.g. ...) => Tag initialization (I1)
#ifdef DEBUG_ON
				printf("PC= %x, 0x%"PRIx64":\t%s\t\t%s // insn-mnem: %s\n",(i->address + 0x8), i->address, i->mnemonic, i->op_str, cs_insn_name(handle, i->id));
				printf("ops = %08x\n", ops);			
#endif				
				if (strncmp(i->mnemonic,"cmp",3) == 0){
					annotation = 0;
					printf("%08x", annotation);
				}
				else if (strncmp(i->mnemonic,"mov",3) == 0){
					if (val_dest[j] == 0x4b){ // r9 register number
						annotation = 0;
						printf("%08x", annotation);					
					}
					else{
						annotation = tagi_r_imm << 26 | val_dest[j]<<21|tag_constants;
						printf("%08x", annotation);					
					}
				}
				else{
					//v1 
					//printf("%08x \n",(val_dest[j])<<28|tag_constants<<24|0x12);
					annotation = tagi_r_imm << 26 | val_dest[j]<<21|tag_constants;
					printf("%08x", annotation);
				}
				break;
			case 0x01: // LDR/STR
				if (strncmp(i->mnemonic,"ldr",3) == 0){
#ifdef DEBUG_ON	
					printf("LDR annotation\n");
#endif
					if (val_dest[j] == 0x4b) // r9 register number
						annotation = 0;
					else 
						annotation = (tag_r_inst << 26 | val_dest[j]<<17 );
					printf("%08x", annotation);
				}
				else if (strncmp(i->mnemonic,"str",3) == 0){ // r9 register number
#ifdef DEBUG_ON
					printf("STR annotation\n");
#endif
				if (val_dest[j] == 0x4b)
					annotation = 0;
				else 
					annotation = (tag_inst_r << 26 | val_dest[j]<<17);
					printf("%08x", annotation);
				}
				else{
					annotation = 0x0;
					printf("%08x", annotation);
				}
				break;
			case 0x31: // dst = reg, src = mem 
				/*** @TODO: RECOVER SIGN OF REGISTER INDEXED MEMORY OFFSET ***/
#ifdef DEBUG_ON			
				printf("PC= %x, 0x%"PRIx64":\t%s\t\t%s // insn-mnem: %s\n",(i->address + 0x8), i->address, i->mnemonic, i->op_str, cs_insn_name(handle, i->id));
				//printf("ops = %08x\n", ops);				
				printf("val_dest[j] = %08x, val_src1 = %08x, annotation_temp=%x\n", val_dest[j], val_src1[j], val_dest[j]<<28 |val_src1[j]|0x84 );
#endif
/*				if ( (val_src1[j]&0x0f000000)>>24 == 0xf )
					printf("%08x \n", ( (val_dest[j])<<28 | val_src1[j] | 0x84) );
				else if ( (val_src1[j]&0x0d000000)>>24 == 0xd )
					printf("%08x \n", ( (val_dest[j])<<28 | val_src1[j] | 0x84) );
				else 
*/					
				//v1
				//printf("%08x \n", ( (val_dest[j])<<28 | val_src1[j] | 0x84) );				
				printf("LDR/STR\n");
				annotation = (ldr[j]==1) ? (tag_r_inst << 26 | val_dest[j]<<17 ): (tag_inst_r << 26 | val_dest[j]<<17);
				//printf("v2 \n 31 mem instruction type %d, annotation = %08x \n", ldr[j], annotation);
				printf("%08x", annotation);
				break;

			case 0x131: // dst = reg, src = mem, src2 = reg (ldrh or strh)
				printf("LDR/STR\n");
				annotation = (ldr[j]==1) ? (tag_r_inst << 26 | val_dest[j]<<17 ): (tag_inst_r << 26 | val_dest[j]<<17);
				printf("%08x", annotation);
				break;
			case 0x2: // branchements directs (b, beq, bXX, bl imm,)
				annotation = 0x0;
				printf("%08x", annotation);
				break;

			case 0x12: // dst = reg, src = reg => tag(dst) = tag(src) (=> MOV instruction e.g. ...) 
				//@DONE: FIXED: @ 0x103ec, no dependencies generated ??? (Memory initalization problem
#ifdef DEBUG_ON
				printf("PC= %x, 0x%"PRIx64":\t%s\t\t%s // insn-mnem: %s\n",(i->address + 0x8), i->address, i->mnemonic, i->op_str, cs_insn_name(handle, i->id));
				printf("ops = %08x\n", ops);
#endif				
				//v1
				//printf("%08x \n",( ((val_dest[j])<<28)==(val_src1[j])<<24 )?0:(val_dest[j])<<28|(val_src1[j])<<24|0x84);
				//annotation = tagi_r_imm << 26 | val_dest[j]<<21|tag_constants;
				//printf("v2 \n %08x \n", annotation);
				break;
			case 0x13: 
				printf("\n\n not dealt in v1\n\n");
			case 0x1211: // LDRD instructions; ldrd rd0, rs1, mem2, rs3
				printf("\n\n not dealt in v1\n\n");
				break;
			case 0x1011: // ldrdeq
				annotation = 0;
				printf("%08x", annotation);
				break;
			case 0x111: // dst = reg, src = reg => tag(dst) = tag(src) (=> MOV instruction e.g. ...) 
				//@DONE: FIXED: @ 0x103ec, no dependencies generated ??? (Memory initalization problem
#ifdef DEBUG_ON
				printf("PC= %x, 0x%"PRIx64":\t%s\t\t%s // insn-mnem: %s\n",(i->address + 0x8), i->address, i->mnemonic, i->op_str, cs_insn_name(handle, i->id));
				printf("ops = %08x\n", ops);
#endif			
				// v1	
				//printf("%08x \n",( (val_dest[j])<<28|(val_src1[j])<<24|(val_src2[j])<<20|0x404) );

				// v2	
				if (strncmp(i->mnemonic,"andeq",5) == 0){
					annotation = 0;
				}
				else if (strncmp(i->mnemonic,"muleq",5) == 0){
					annotation = 0;				
				}
				else{
					annotation = (tagu_rrr_o << 26 |(val_dest[j])<<17|(val_src1[j])<<12|(val_src2[j])<<7|FUNC_OR);
				}				
				printf("%08x", annotation);
				break;

			case 0x211: // e.g. sub sp, sp, #30 => general operation to be done on the Coprocessor as well ...
				//v1
				//printf("%08x \n",( ((val_dest[j])<<28)==(val_src1[j])<<24 )?0:(val_dest[j])<<28|(val_src1[j])<<24|0x84);
				annotation = ( val_dest[j] == val_src1[j] ) ? 0 : (tagu_rrr_o << 26 | (val_dest[j])<<17|(val_src1[j])<<12|FUNC_OR);
				if (val_dest[j]==val_src1[j] && val_dest[j]==13) // sp related instruction (add, sub or other ...)
					annotation = tagu_rrr_o << 26 | 0x1e << 17 | 0x1e << 12 | 0;
				//(ldr[j]==1) ? annotation = (tag_r_inst << 26 | val_dest[j]<<17 ): (tag_inst_r << 26 | val_dest[j]<<17);
				printf("%08x", annotation);
#ifdef DEBUG_ON
				printf("PC= %x, 0x%"PRIx64":\t%s\t\t%s // insn-mnem: %s\n",(i->address + 0x8), i->address, i->mnemonic, i->op_str, cs_insn_name(handle, i->id));
				printf("ops = %08x\n", ops);
#endif
				break;
			case 0x231: // e.g. ldr r3, [r5], #4 => @ 10618				
				// ldr[j] == 1 for load instructions, 0 for store instructions and 2 for all others. 
				annotation = (ldr[j]==1) ? (tag_r_inst << 26 | val_dest[j]<<17 ): (tag_inst_r << 26 | val_dest[j]<<17);
				printf("%08x", annotation);
				//printf("v2 \n 231 mem instruction type %d, %08x \n", ldr[j], annotation);
#ifdef DEBUG_ON
				printf("ops = %x\n", ops);
				printf("val_dest[j] = %08x, val_src1 = %08x, annotation_temp = %x\n", val_dest[j], val_src1[j], val_dest[j]<<28 |val_src1[j]|0x84 );	
				printf("PC= %x, 0x%"PRIx64":\t%s\t\t%s // insn-mnem: %s\n",(i->address + 0x8), i->address, i->mnemonic, i->op_str, cs_insn_name(handle, i->id));
				printf("ops = %08x\n", ops);
#endif
				break;
			case 0x311: // (ldrd) and strd instructions => two operations for this instruction
				annotation = (ldr[j]==1) ? (tag_r_inst << 26 | val_dest[j]<<17 ): (tag_inst_r << 26 | val_dest[j]<<17);
				printf("%08x", annotation);
				annotation = (ldr[j]==1) ? (tag_r_inst << 26 | val_src1[j]<<17 ): (tag_inst_r << 26 | val_src1[j]<<17);
				printf("%08x", annotation);				
			case 0xF0000000: //push
				push_counter_write++;
#ifdef DEBUG_ON			
				printf("PC= %x, 0x%"PRIx64":\t%s\t\t%s // insn-mnem: %s\n",(i->address + 0x8), i->address, i->mnemonic, i->op_str, cs_insn_name(handle, i->id));
				//printf("ops = %08x\n", ops);				
				//printf("push_annotations_temp : %08x \n",operands_push[push_counter_write]);
#endif
				push_annotations = operands_push[push_counter_write];
				// printf("PUSH ANNOTATION START %08x\n",push_annotations);
				if (push_annotations & 0x1){
					annotation = (tag_inst_r << 26 | 0<<17);
					printf("%08x \t", annotation);
				}
				if (push_annotations & 0x2){
					annotation = (tag_inst_r << 26 | 1<<17);
					printf("%08x \t", annotation);
				}
				if (push_annotations & 1<<2){
					annotation = (tag_inst_r << 26 | 2<<17);
					printf("%08x \t", annotation);
				}
				if (push_annotations & 1<<3){
					annotation = (tag_inst_r << 26 | 3<<17);
					printf("%08x \t", annotation);
				}
				if (push_annotations & 1<<4){
					annotation = (tag_inst_r << 26 | 4<<17);
					printf("%08x \t", annotation);
				}
				if (push_annotations & 1<<5){
					annotation = (tag_inst_r << 26 | 5<<17);
					printf("%08x \t", annotation);
				}
				if (push_annotations & 1<<6){
					annotation = (tag_inst_r << 26 | 6<<17);
					printf("%08x \t", annotation);	
				}
				if (push_annotations & 1<<7){
					annotation = (tag_inst_r << 26 | 7<<17);
					printf("%08x \t", annotation);
				}
				if (push_annotations & 1<<8){
					annotation = (tag_inst_r << 26 | 8<<17);
					printf("%08x \t", annotation);
				}
				if (push_annotations & 1<<9){
					annotation = (tag_inst_r << 26 | 9<<17);
					printf("%08x \t", annotation);
				}
				if (push_annotations & 1<<10){
					annotation = (tag_inst_r << 26 | 10<<17);
					printf("%08x \t", annotation);
				}
				if (push_annotations & 1<<11){
					annotation = (tag_inst_r << 26 | 11<<17);
					printf("%08x \t", annotation);
				}
				if (push_annotations & 1<<12){
					annotation = (tag_inst_r << 26 | 12<<17);
					printf("%08x \t", annotation);
				}
				if (push_annotations & 1<<13){
					annotation = (tag_inst_r << 26 | 13<<17);
					printf("%08x \t", annotation);
				}
				if (push_annotations & 1<<14){
					annotation = (tag_inst_r << 26 | 14<<17);
					printf("%08x \t", annotation);
				}
				if (push_annotations & 1<<15){
					annotation = (tag_inst_r << 26 | 15<<17);
					printf("%08x \t", annotation);	
				}				
				//printf("PUSH ANNOTATION END");
				printf("\n");
				break;
			case 0xE0000000 : //pop
				pop_counter_write++;
#ifdef DEBUG_ON			
				printf("PC= %x, 0x%"PRIx64":\t%s\t\t%s // insn-mnem: %s\n",(i->address + 0x8), i->address, i->mnemonic, i->op_str, cs_insn_name(handle, i->id));
				//printf("ops = %08x\n", ops);				
				//printf("pop_annotations_temp : %08x \n",operands_pop[pop_counter_write]);
#endif
				pop_annotations = operands_pop[pop_counter_write];
				// printf("POP ANNOTATION START %08x\n",pop_annotations);
				if (pop_annotations & 0x1){
					annotation = (tag_r_inst << 26 | 0<<17);
					printf("%08x \t", annotation);
				}
				if (pop_annotations & 0x2){
					annotation = (tag_r_inst << 26 | 1<<17);
					printf("%08x \t", annotation);
				}
				if (pop_annotations & 1<<2){
					annotation = (tag_r_inst << 26 | 2<<17);
					printf("%08x \t", annotation);
				}
				if (pop_annotations & 1<<3){
					annotation = (tag_r_inst << 26 | 3<<17);
					printf("%08x \t", annotation);
				}
				if (pop_annotations & 1<<4){
					annotation = (tag_r_inst << 26 | 4<<17);
					printf("%08x \t", annotation);
				}
				if (pop_annotations & 1<<5){
					annotation = (tag_r_inst << 26 | 5<<17);
					printf("%08x \t", annotation);
				}
				if (pop_annotations & 1<<6){
					annotation = (tag_r_inst << 26 | 6<<17);
					printf("%08x \t", annotation);	
				}
				if (pop_annotations & 1<<7){
					annotation = (tag_r_inst << 26 | 7<<17);
					printf("%08x \t", annotation);
				}
				if (pop_annotations & 1<<8){
					annotation = (tag_r_inst << 26 | 8<<17);
					printf("%08x \t", annotation);
				}
				if (pop_annotations & 1<<9){
					annotation = (tag_r_inst << 26 | 9<<17);
					printf("%08x \t", annotation);
				}
				if (pop_annotations & 1<<10){
					annotation = (tag_r_inst << 26 | 10<<17);
					printf("%08x \t", annotation);
				}
				if (pop_annotations & 1<<11){
					annotation = (tag_r_inst << 26 | 11<<17);
					printf("%08x \t", annotation);
				}
				if (pop_annotations & 1<<12){
					annotation = (tag_r_inst << 26 | 12<<17);
					printf("%08x \t", annotation);
				}
				if (pop_annotations & 1<<13){
					annotation = (tag_r_inst << 26 | 13<<17);
					printf("%08x \t", annotation);
				}
				if (pop_annotations & 1<<14){
					annotation = (tag_r_inst << 26 | 14<<17);
					printf("%08x \t", annotation);
				}
				if (pop_annotations & 1<<15){
					annotation = (tag_r_inst << 26 | 15<<17);
					printf("%08x \t", annotation);	
				}				
				// printf("POP ANNOTATION END");				
				//printf("%08x\n", ( pop_annotations | 0x1004));
				break;
		}
		printf("\n");
	}
	cs_free(insn, count);
}
	cs_close(&handle);
	return 0;
}
