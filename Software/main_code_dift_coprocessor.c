#define Xil_In32(Addr)  		(*(volatile unsigned *)(Addr))
#define Xil_Out32(Addr, Value)	(*(volatile unsigned *)((Addr)) = (Value))
#define ANNOTATION_ADDRESS 		(0x10000)
#define ANNOTATION_BB_ADDRESS	(0x20000)
#define TRACE_ADDRESS			(0x30000)


// Configure 1st stage 
static inline void configure_first_stage(){
}

// Configure 2nd stage 
static inline void configure_second_stage(){
	// configure security policy registers (TPRs and TCRs)

	// configure MMU 

}

// Fetch annotations/dependencies jump table from Memory
// @TODOs:improvement possible: store it in an associative memory block 
static inline void fetch_jump_table(){
	// read annotations at 0x1800 0000 to determine size of annotations
	
}

// Search annotations for given address in argument and return the address of anntoations in memory for corresponding address
static inline unsigned int search_annotations_address(unsigned int address){
	unsigned int addr, i, ret = 0, temp;
	unsigned jump_table_size = Xil_In32(ANNOTATION_ADDRESS+0);
	// read jump table
	for (i = 1; i < jump_table_size; i+=2){
		temp = i*4;
		if (address == Xil_In32(ANNOTATION_ADDRESS + temp)){
			ret = Xil_In32(ANNOTATION_ADDRESS+temp+4);
			break;
		}
	}
	return ret;
}

inline void quit(){
	while(1){
	}
}

// Store annotations in memory for the corresponding trace 
static inline void store_annotations_memory(unsigned int offset, unsigned int size, unsigned int start_write_address){
	unsigned int i, annotation; 
	unsigned write_address, temp;
	unsigned annotation_offset = ANNOTATION_ADDRESS + offset*4;
	write_address = start_write_address;
	for (i = 0; i < size; i++){
		temp = i*4;
		annotation = Xil_In32(annotation_offset + temp);
		write_address += 4;
		Xil_Out32(write_address, annotation);
		/*
		 * Deal here with RFBLARE PS2PL and RFBLARE PL2PS IPs 
		 * 
		 *
		 */
	}
}



int main(){
	unsigned int count_trace = 1;
	unsigned int annotation_size, annotation_offset;
	unsigned int trace;
	//unsigned int *go = &ADDRESS, done = 0;
	
	// Make sure that tracing have been started before carrying on
	// Polling method => @TODO: Replace it by interrupt 
	unsigned int start_value = 0, first_trace_address, write_address;
	write_address = ANNOTATION_BB_ADDRESS;
	while(1){
		first_trace_address = TRACE_ADDRESS + 0x4;
		start_value = Xil_In32(first_trace_address);
		if (start_value != 0)
			break;
	}

	//configure_first_stage();
	//configure_second_stage();
	
	/*
	 * Load configuration IP to config memory
	 * Initialize MMU with process mappings 
	 * OR MMU automatically adds entries ...
	 * Fetch all annotations from DDR to local mem to gain in mem access
	 * Start LOOP below ...
	 * */

	// 0. Fetch jump table from DDR memory
	//fetch_jump_table();

	// 0. Configure the CPU (first and second stages)	
	while(1){
		/** 1. Read trace from the trace BRAM located at @ 0xC000_0000 **/
		trace = Xil_In32(TRACE_ADDRESS+(count_trace*4));
		if (trace == 0)
			break;
		count_trace++;
//		if (trace != 0) { // ignore null traces
		// search for annotations	
		annotation_offset = search_annotations_address(trace);
		if (annotation_offset != 0){ // annotation_offset = 0 means there are no annotations for this basic block
			// Determine annotation size by reading header at annotation_address
			annotation_size = Xil_In32(ANNOTATION_ADDRESS+annotation_offset*4);
			annotation_offset += 1;
			// Store annotations in memory for second stage
			store_annotations_memory(annotation_offset, annotation_size,write_address);
			//annotation_offset += annotation_size;
			write_address += annotation_size*4;
			// start the second stage 
			//*go = 1;
			// determine how many entries to skip and skip them 
			//
			//count_trace += ;
			// // when done signal received from second stage, redo the same loop
			// if (done = 1){
			//	break;
			// }
		}
		if (count_trace == 2) 
			break;
	}
	quit();
	return 0;
}

