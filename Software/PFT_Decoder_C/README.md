This file contains instructions to understand the usage of PFT decoder.
# convert trace text to binary file 
xxd -r -p trace.txt > trace.bin


# use ptm decoder to obtain decoded trace
../ptm_decode.o --pft-1.1 --cycle-accurate 0 --print-input --contextid-bytes 0 --input trace.bin --print-config > trace.decoded

The options given to the program `ptm_decode.o` means the following. 

- pfg-1.1 option specifies the PFT architecture version used. 
- cycle-accurate option specifies whether the cycle accurate feature has been enabled when tracing. - print-input option displays each packet input values.
- contextid-bytes option specifies the configuration of context id bytes when CoreSight components were configured  during tracing. 
- The input option recover the trace binary file obtained using `xxd`. 
- print-config shows the configuration option.

