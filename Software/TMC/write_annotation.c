#include <stdio.h>
#include <stdint.h>

int main(int argc, char **argv[]){
    uint32_t annotation;
    unsigned zero = 0;
    uint32_t data;
    uint32_t upper_data;
    uint32_t lower_data;
    char *ptr;
    if (argc == 2) {
    	printf("The argument supplied is %s.\n", (char *)argv[1]);
    }
    else if (argc > 2){
    	printf("Too many arguments supplied.\n");
    }
    else{
    	printf("One argument is expected.\n");
    }
    data = strtoul(argv[1], &ptr, 16); // hexadecimal base
    upper_data = data >> 16;
    lower_data = data & 0x0000ffff;
    printf("upper data is %x\n", upper_data);
    printf("lower data is %x\n", lower_data);
    annotation = 0x20<<26 | 0x01 << 21 | 0x01<<16 | lower_data; // tag_reg_imm instruction
    printf("%08x\n", annotation);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    annotation = 0x2b<<26 | 0x01 << 21 | 0x01<<16 | upper_data; // lui instruction
    printf("%08x\n", annotation);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    annotation = 0x2c<<26 | 0x10 << 17 | 0x01<<12 | 0; // tag_mem_tr_2 instruction
    printf("%08x\n", annotation);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
	
    return 0;
}
