#include <stdio.h>
#include <stdint.h>

int main(int argc, char **argv[]){
    uint32_t annotation;
    unsigned zero = 0;
    uint32_t address;
    uint32_t upper_address;
    uint32_t lower_address;
    char *ptr;
    if (argc == 2) {
    	printf("The argument supplied is %s.\n", (char *)argv[1]);
    }
    else if (argc > 2){
    	printf("Too many arguments supplied.\n");
    }
    else{
    	printf("One argument is expected.\n");
    }
    address = strtoul(argv[1], &ptr, 16); // hexadecimal base
    upper_address = address >> 16;
    lower_address = address & 0x0000ffff;
    printf("upper address is %x\n", upper_address);
    printf("lower address is %x\n", lower_address);
    annotation = 0x20<<26 | 0x10 << 21 | 0x10<<16 | lower_address; // tag_reg_imm instruction
    printf("%08x\n", annotation);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    annotation = 0x2b<<26 | 0x10 << 21 | 0x10<<16 | upper_address; // lui instruction
    printf("%08x\n", annotation);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    // write a TLB entry and add nops to make sure that everything is fine ....
    annotation = 0x4<<26|0x10<<16;
    printf("%08x\n", annotation);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);
    printf("%08x\n", zero);

    return 0;
}
