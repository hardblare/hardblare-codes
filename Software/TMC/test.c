#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main(){

	int num, count;
	char *buf = malloc(32*sizeof(int));
	int fr, fw ;

	fr = open("read.txt", O_RDONLY);
	fw = open("write.txt", O_WRONLY);

	count = read(fr, buf, 32*sizeof(char));
	write(fw, buf, count*sizeof(char));

	return 0;
}
