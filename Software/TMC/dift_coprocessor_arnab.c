/*-------------------------------------------------------------------
-- TITLE: DIFT coprocessor
-- AUTHOR: MAW
-- DATE CREATED: 03/11/2017
-- FILENAME: dift_coprocessor.c
-- PROJECT: MIPS CPU core
-- DESCRIPTION:
--   DIFT Coprocessor dependencies generation
--------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
#include <unistd.h>

#define SIZE   1

unsigned used_opcodes[]={0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c,0x32,0x34,0x35};

//long generate_dependency(unsigned int expected_opcode);

/************************************************************/

/* Generate random number between range 
* [src](https://stackoverflow.com/questions/2509679/how-to-generate-a-random-number-from-within-a-range) 
*/
// Assumes 0 <= max <= RAND_MAX
// Returns in the closed interval [0, max]
long random_at_most(long max) {
	unsigned long
		// max <= RAND_MAX < ULONG_MAX, so this is okay.
	num_bins = (unsigned long) max + 1,
	num_rand = (unsigned long) RAND_MAX + 1,
	bin_size = num_rand / num_bins,
	defect   = num_rand % num_bins;

	long x;
	do {
		x = random();
	}
	// This is carefully written not to overflow
	while (num_rand - defect <= (unsigned long)x);

	// Truncated division is intentional
	return x/bin_size;
}

// Generate a dependency based on the opcode and format chosen ! 
long generate_dependency(unsigned int expected_opcode){
	long dependency; 
	int rd_tag, rd_compute, rd;
	int rs_tag, rs_compute, rs;
	int rs2_tag, rs2_compute;
	int imm_12, imm_16;
	int arm_opcode_type = 0, func = 0;

	rd = random_at_most(23);
	rd_tag = random_at_most(16); // generate a random number between 0 and 15
	rd_compute = 16 + random_at_most(8); // generate a random number between 16 and 23
	rs_tag = random_at_most(16); // generate a random number between 0 and 15
	rs2_tag = random_at_most(16); // generate a random number between 0 and 15
	rs_compute = 16 + random_at_most(8); // generate a random number between 16 and 23
	imm_12 = random_at_most(pow(2,12)); // generate a random number between 0 and 2**12(4096)
	imm_16 = random_at_most(pow(2,16)); // generate a random number between 0 and 2**16(65536)
	func = random_at_most(8);

	switch(expected_opcode){
		/** Tag initialization **/
	//	case 0x1: // LW
	//	dependency = 0x1	
		case 0x20:  // tag(reg_dst) <= imm_16;
		dependency = 0x20<<26 | rd << 21 | imm_16; 
		break;
		case 0x21:  // // tag(reg_dst) <= reg_src;
		dependency = 0x21<<26 | rd_tag << 21 | rs_compute << 16;
		break;
		case 0x22:  
		dependency = 0x22<<26 | rd_tag << 21 | rs << 16 | imm_16; 
		break;
		case 0x32: 
		dependency = 0x32<<26 | rd_compute << 21 | rs_compute << 16 | imm_16; 
		break;
		

		/** Tag propagate **/
		case 0x23: 
		dependency = 0x23<<26 | arm_opcode_type<<22 | rd_tag << 17 | rs_tag << 12 | rs2_tag << 7 ; 
		break;
		case 0x24: 
		dependency = 0x24<<26 | arm_opcode_type<<22 | rd_compute << 17 | rs_tag << 12 | rs2_tag << 7 ; 
		break;
		case 0x25: 
		dependency = 0x25<<26 | arm_opcode_type<<22 | rd_tag << 17 | rs_compute << 12 | imm_12 ; 
		break;
		case 0x26: 
		dependency = 0x26<<26 | arm_opcode_type<<22 | rd_compute << 17 | rs_compute << 12 | rs2_compute << 7 | func ; 
		break;
		case 0x34: 
		dependency = 0x34<<26 | arm_opcode_type<<22 | rd_compute << 17 | rs_tag << 12 | rs2_tag << 7 ; 
		break;
		case 0x35: 
		dependency = 0x35<<26 | arm_opcode_type<<22 | rd_tag << 17 | rs_compute << 12 | imm_12 ; 
		break;
		case 0x27: 
		dependency = 0x27<<26 | arm_opcode_type<<22 | 0 << 17 | rs_tag << 12 | 0 ; 
		break;
		case 0x28: 
		dependency = 0x28<<26 | arm_opcode_type<<22 | rd_tag << 17 | 0 << 12 | 0 ; 
		break;
		case 0x29: 
		dependency = 0x29<<26 | arm_opcode_type<<22 | 0 << 17 | rs_tag << 12 | 0 ; 
		break;
		case 0x2a: 
		dependency = 0x2a<<26 | arm_opcode_type<<22 | 0 << 17 | rs_tag << 12 | 0 ; 
		break;
		/** Tag check **/
		default:
		dependency = 0;
		break;
	}
	 
	 printf("rd_tag = %d, rd_compute = %d\n", rd_tag, rd_compute);
	 printf("rs_tag = %d, rs_compute = %d\n", rs_tag, rs_compute);
	 printf("rs2_tag = %d, rs2_compute = %d\n", rs2_tag, rs2_compute);
	 printf("imm_12 = %d, imm_16 = %d\n", imm_12, imm_16);
	 printf("-----\n");

	return dependency;
}

// Generate a dependency based on the opcode and format chosen ! 
long generate_dependency_u(unsigned int expected_opcode){
	long dependency; 
	static int rd_tag = 0, rd_compute = 15, rd = 0;
	static int rs_tag = 0, rs_compute = 15, rs = 0;
	static int rs2_tag, rs2_compute = 18;
	static int imm_12, imm_16 = 4096, func ;
	static int arm_opcode_type = 1;
	rd++;	
	rd_tag++;
	rd_compute++;
	rs_tag++;
	rs2_tag++;
	rs_compute++;
	imm_12++;
	imm_16++;
	rs2_compute++;
	rs++;
	func++;
	//arm_opcode_type++;
	if (arm_opcode_type > 0xa)
		arm_opcode_type = 0;
	if (rd_tag == 0x32)
		rd_tag = 0x0;
	if (rd_compute == 24) 
		rd_compute = 16;
	if (rs_compute == 24) 
		rs_compute = 16;
	if (rs2_compute == 24) 
		rs2_compute = 16;
	switch(expected_opcode){
		case 0x01:
		dependency = (expected_opcode)<<26 | rd_compute << 21 | rs_compute << 16 | imm_16;
		break;
		case 0x02:
		dependency = (expected_opcode)<<26 | rd_compute << 21 | rs_compute << 16 | imm_16;
		break;
		case 0x03:
		dependency = (expected_opcode)<<26 | rd_compute << 21 | rs_compute << 16 | imm_16;
		break;
		/** Tag initialization **/
		case 0x20:  // tag(reg_dst) <= imm_16;
		dependency = (expected_opcode)<<26 | rd << 21 | imm_16;
		break;
		case 0x21:  // // tag(reg_dst) <= reg_src;
		dependency = (expected_opcode)<<26 | rd_tag << 21 | rs_compute << 16;
		break;
		case 0x22:  
		dependency = (expected_opcode)<<26 | rd_tag << 21 | rs << 16 | imm_16; 
		break;
		case 0x2b: 
		dependency = (expected_opcode)<<26 | rd_compute << 21 | rs_compute << 16 | imm_16; 
		break;
		case 0x32: 
		dependency = (expected_opcode)<<26 | rd_compute << 21 | rs_compute << 16 | imm_16; 
		break;
		

		/** Tag propagate **/
		case 0x23: 
		dependency = (expected_opcode)<<26 | arm_opcode_type<<22 | rd_tag << 17 | rs_tag << 12 | rs2_tag << 7 ; 
		break;
		case 0x24: 
		dependency = (expected_opcode)<<26 | arm_opcode_type<<22 | rd_tag << 17 | rs_compute << 12 | imm_12 ; 
		break;
		case 0x25: 
		dependency = (expected_opcode)<<26 | arm_opcode_type<<22 | rd_tag << 17 | rs_compute << 12 | imm_12 ; 
		break;
		case 0x26: 
		dependency = (expected_opcode)<<26 | arm_opcode_type<<22 | rd_compute << 17 | rs_compute << 12 | rs2_compute << 7 | func ; 
		break;
		case 0x2c: 
		dependency = (expected_opcode)<<26 | arm_opcode_type<<22 | rd_compute << 17 | rs_tag << 12 | imm_12 ; 
		break;
		case 0x2d: 
		dependency = (expected_opcode)<<26 | arm_opcode_type<<22 | rd_tag << 17 | rs_compute << 12 | imm_12 ; 
		break;
		case 0x27: 
		dependency = (expected_opcode)<<26 | arm_opcode_type<<22 | 0 << 17 | rs_tag << 12 | 0 ; 
		break;
		case 0x28: 
		dependency = (expected_opcode)<<26 | arm_opcode_type<<22 | rd_tag << 17 | 0 << 12 | 0 ; 
		break;
		case 0x29: 
		dependency = (expected_opcode)<<26 | arm_opcode_type<<22 | 0 << 17 | rs_tag << 12 | 0 ; 
		break;
		case 0x2a: 
		dependency = (expected_opcode)<<26 | arm_opcode_type<<22 | 0 << 17 | rs_tag << 12 | 0 ; 
		break;
		/** Tag check **/
		default:
		dependency = 0;
		break;
	}
	printf("rd_tag = %d, rd_compute = %d, rd = %u\n", rd_tag, rd_compute, rd);
	printf("rs_tag = %d, rs_compute = %d, rs = %d\n", rs_tag, rs_compute, rs);
	printf("rs2_tag = %d, rs2_compute = %d\n", rs2_tag, rs2_compute);
	printf("imm_12 = %d, imm_16 = %d\n", imm_12, imm_16);
	printf("ARM opcode Type = %x\n",arm_opcode_type);
	printf("Func = %x\n",func);
	printf("-----\n");
	return dependency;
}


// print generated dependencies
void print_dependencies(unsigned int *p, unsigned size){
	int i;
	for (i = 0; i < size; i++)
		printf("%x\n", p[i]);
}

int main(int argc,char *argv[]){
	unsigned int *dependencies, i, opcode, nb_opcodes; 
	char *end_ptr;
	unsigned int base = 16;
	long val;
	dependencies = calloc(2*SIZE, sizeof(unsigned int)); 
	if (argc != 2){
		printf("The program requires one argument\nusage: %s opcode\n",argv[0]);
		exit(EXIT_FAILURE);
	}
	else
		val = strtoul(argv[1],&end_ptr,base);
	
	opcode = val;
	/*
 nb_opcodes = sizeof(used_opcodes)/sizeof(unsigned);
 int out_of_while = 0;
 do{
		opcode = 0x20 + random_at_most(0x20);
		for (i = 0; i < nb_opcodes; i++)
			 if (opcode == *(used_opcodes+i))
					printf("opcode = %d\n", opcode), out_of_while = 1;
 }while(out_of_while != 1);
*/
/*
 for (i = 0; i < SIZE; i++)
		*(dependencies+i) = generate_dependency(0x20);
*/

	for (i = 0; i < SIZE; i++){
		//if (i < SIZE)
			*(dependencies+i) = generate_dependency_u(opcode);
		//else
		//	*(dependencies+i) = generate_dependency_u(0x21);
	}

	print_dependencies(dependencies, SIZE);
	char cmd[60];
	sprintf(cmd,"cat /proc/%d/maps > %s.maps",getpid(),argv[0]);
	system(cmd);
	return(0);
}

