All projects are compressed. They need to be unzipped first and then opened using Vivado 2017.1 tools. They are adaptable to any version but some IPs may need to be packaged using the targeted version of the tools.

| File/**Folder** name | Desription | Remarks | 
| ---------------- | ---------- | ------- |
| **images** | This folder contains images for the README file | |
| dispatcher.xpr.zip | This project contains the MIPS core used as the dispatcher. | The C code that must be running on the dispatcher can be found in the Software folder. |
| system_design.xpr.zip | This project contains the full system developed in this thesis. | Some IPs may need to be upgraded. The design helps with the connections and the understanding of the overall architecture. |

## **system_design** project

![](images/full_system_design.png)

### **Dispatcher** Sub-design in **system_design** project

![](images/dispatcher_core_vivado_design_full.png)

### **TMC** Sub-design in **system_design** project

![](images/tmc_core_vivado_design_full.png)

## **dispatcher** project

![](images/dispatcher_design_fpga.png)